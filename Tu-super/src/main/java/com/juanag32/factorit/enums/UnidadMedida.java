package com.juanag32.factorit.enums;

public enum UnidadMedida {
    KILOGRAMO,
    GRAMO,
    LITRO,
    MILILITRO,
    UNIDAD,
    PAQUETE,
    CAJA

}
