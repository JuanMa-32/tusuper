package com.juanag32.factorit.Hilos;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class HiloEmail implements Runnable{
    private String email;
    private JavaMailSender sendMail;

    public HiloEmail(String email,JavaMailSender sendMail) {
        this.email = email;
        this.sendMail = sendMail;
    }

    @Override
    public void run() {
        SimpleMailMessage message = new SimpleMailMessage();

        String mail = "Te felicitamos eres cliente VIP!! \n" +
                "Apartir de ahora recibiras bonificaciones en tus compras  \n" +
                "2. Contraseña: impacto2024  (copia y pega la contraseña sin espacios)";

        message.setFrom("juanmaguero4@gmail.com");
        message.setTo(email);
        message.setText(mail);
        message.setSubject("¡Eres Cliente VIP!");
        sendMail.send(message);
    }

}