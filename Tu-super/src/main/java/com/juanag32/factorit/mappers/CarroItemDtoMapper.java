package com.juanag32.factorit.mappers;

import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.entities.CarroItem;

public class CarroItemDtoMapper {

    CarroItem carroItem;

    private CarroItemDtoMapper() {
    }

    public static CarroItemDtoMapper getInstance() {
        return new CarroItemDtoMapper();
    }

    public CarroItemDtoMapper setCarroItem(CarroItem carroItem) {
        this.carroItem = carroItem;
        return this;
    }

    public CarroItemDto build() {
        ProductoDto productoDto = ProductoDtoMapper.getInstance().setProducto(carroItem.getProducto()).build();
        CarroItemDto dto = new CarroItemDto(carroItem.getId(), productoDto, carroItem.getCantidad(), carroItem.getPrecioUnitario());
        return dto;
    }
}
