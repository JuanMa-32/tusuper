package com.juanag32.factorit.mappers;

import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.entities.Cliente;

public class ClienteDtoMapper {
    private Cliente client;

    private ClienteDtoMapper() {
    }

    public static ClienteDtoMapper getInstance(){
        return new ClienteDtoMapper();
    }

    public ClienteDtoMapper setCliente(Cliente cliente){
        this.client = cliente;
        return this;
    }

    public ClienteDto build(){
        ClienteDto dto=new ClienteDto(client.getId(),client.getNombre(), client.getApellido(),
                client.getEmail(),client.isVip(),client.getComienzoVip(),client.getFinalVip());
        return dto;
    }
}
