package com.juanag32.factorit.mappers;

import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.entities.Usuario;

public class UsuarioDtoMapper {
    private Usuario usuario;

    private UsuarioDtoMapper() {
    }

    public static UsuarioDtoMapper getInstance() {
        return new UsuarioDtoMapper();
    }

    public UsuarioDtoMapper setUsuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public UsuarioDto build(){
        UsuarioDto usuarioDto = new UsuarioDto(usuario.getId(), usuario.getNombre(),
                usuario.getApellido(), usuario.getEmail(), usuario.getRole().toString());
        return  usuarioDto;
    }

}
