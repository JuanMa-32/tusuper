package com.juanag32.factorit.mappers;

import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.entities.Carro;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class CarroDtoMapper {
    private Carro carro;

    private CarroDtoMapper() {
    }
    public static CarroDtoMapper getInstance(){
        return new  CarroDtoMapper();
    }
    public CarroDtoMapper setCarro(Carro carro){
        this.carro=carro;
        return  this;
    }
    public CarroDto build(){
        //convertimos todos los item del carro en itemDto usando stream.
        List<CarroItemDto> carroItemDtoList = carro.getItems().stream().map(i -> CarroItemDtoMapper.getInstance().setCarroItem(i).build()).collect(Collectors.toList());

        CarroDto dto = new CarroDto(carro.getId(), carro.getFechaCompra(),carro.getSubtotal(),carro.getDescuento(),carro.getTotal(),
                carro.getTipoCarro().toString(),carroItemDtoList);
        return dto;
    }
}
