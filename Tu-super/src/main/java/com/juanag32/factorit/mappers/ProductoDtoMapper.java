package com.juanag32.factorit.mappers;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.entities.Producto;

public class ProductoDtoMapper {
    Producto producto;

    private ProductoDtoMapper() {
    }
    public static ProductoDtoMapper getInstance(){
        return new ProductoDtoMapper();
    }
    public ProductoDtoMapper setProducto(Producto producto){
        this.producto=producto;
        return this;
    }


    public ProductoDto build(){
        ProductoDto dto = new ProductoDto(producto.getId(), producto.getNombre(), producto.getPrecio(),
               producto.getCategoria().toString(), producto.getUnidadMedida().toString(), producto.getMedidaValor(), producto.isActivo());

        return dto;
    }
}
