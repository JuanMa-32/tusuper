package com.juanag32.factorit.auth;


import com.juanag32.factorit.auth.filter.JwtAuthenticationFilter;
import com.juanag32.factorit.auth.filter.JwtValidationFilter;
import com.juanag32.factorit.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
public class SecurityConfig {

    private final UsuarioRepository usuarioRepository;
    @Autowired
    private AuthenticationConfiguration authenticationConfiguration;
    public SecurityConfig(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Bean
    AuthenticationManager authenticationManager() throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.authorizeHttpRequests(authRules -> authRules
                        .requestMatchers(HttpMethod.GET, "/api/carro/carros/client/{idCliente}").hasRole("CLIENT")
                        .requestMatchers(HttpMethod.GET, "/api/carro/{id}").hasRole("CLIENT")
                        .requestMatchers(HttpMethod.POST, "/api/carro/save").hasRole("CLIENT")
                        .requestMatchers(HttpMethod.POST, "/api/dia-promocion/save").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.POST, "/api/usuarios/save").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.GET, "/api/clientes/clientes-vip").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.GET, "/api/clientes/{id}").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/clientes/save").permitAll()
                        .requestMatchers(HttpMethod.PUT, "/api/clientes/cancelar-vip/{id}").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.GET, "/api/productos/activos").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/productos/all").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/productos/buscar-categoria/{categoria}").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/productos/precio-asc").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/productos/precio-desc").permitAll()
                        .requestMatchers(HttpMethod.GET, "/api/productos/{nombre}").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/productos/save").hasRole("ADMIN")
                        .requestMatchers(HttpMethod.PUT, "/api/productos/cambiar-estado/{id}").hasRole("ADMIN")
                        .requestMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html").permitAll()
                        .anyRequest().authenticated())
                .addFilter(new JwtAuthenticationFilter(authenticationConfiguration.getAuthenticationManager(),usuarioRepository))
                .addFilter(new JwtValidationFilter(authenticationConfiguration.getAuthenticationManager()))
                .csrf(config -> config.disable())
                .sessionManagement(managment -> managment.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .build();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(Arrays.asList("http://localhost:5173"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
        config.setAllowedHeaders(Arrays.asList("Authorization", "Content-Type"));
        config.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    FilterRegistrationBean<CorsFilter> corsFilter() {
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(corsConfigurationSource()));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
