package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.entities.DiaPromocion;
import com.juanag32.factorit.repositories.DiaPromocionRepository;
import com.juanag32.factorit.services.DiaPromocionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@Service
public class DiaPromocionServiceImpl implements DiaPromocionService {
    @Autowired
    private DiaPromocionRepository repository;

    @Override
    @Transactional
    public DiaPromocion save(DiaPromocion diaPromocion) {

        return repository.save(diaPromocion);
    }

    @Override
    @Transactional(readOnly = true)
    public DiaPromocion findByFecha(LocalDate fecha) {
        DiaPromocion diaPromocion = repository.findByFecha(fecha).orElseThrow(()->new NoSuchElementException("No hay un descuento en esta fecha"));
        return diaPromocion;
    }
}
