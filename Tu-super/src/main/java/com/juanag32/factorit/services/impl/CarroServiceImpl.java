package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.Hilos.HiloEmail;
import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carro.CarroRequestDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemRequestDto;
import com.juanag32.factorit.entities.Carro;
import com.juanag32.factorit.entities.CarroItem;
import com.juanag32.factorit.entities.Cliente;
import com.juanag32.factorit.entities.Producto;
import com.juanag32.factorit.enums.Categoria;
import com.juanag32.factorit.enums.TipoCarro;
import com.juanag32.factorit.mappers.CarroDtoMapper;
import com.juanag32.factorit.repositories.CarroRepository;
import com.juanag32.factorit.repositories.ClienteRepository;
import com.juanag32.factorit.repositories.ProductoRepository;
import com.juanag32.factorit.services.CarroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CarroServiceImpl implements CarroService {
    @Autowired
    private CarroRepository repository;
    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private JavaMailSender sendMail;

    @Override
    @Transactional(readOnly = true)
    public List<CarroDto> findAllByClient(Long idCliente) {
        List<CarroDto> carroDtos = repository.findAllByClienteId(idCliente).stream().map(c -> CarroDtoMapper.getInstance().setCarro(c).build()).collect(Collectors.toList());
        return carroDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public CarroDto findById(Long id) {
        Carro carro = repository.findById(id).orElseThrow(() -> new NoSuchElementException("el carro de compras no fue encontrado"));
        CarroDto dto = CarroDtoMapper.getInstance().setCarro(carro).build();
        return dto;
    }

    @Override
    @Transactional
    public CarroDto save(CarroRequestDto carroRequestDto) {
        Carro carro = new Carro();
        TipoCarro tipoCarro = TipoCarro.valueOf(carroRequestDto.getTipoCarro().toUpperCase());
        carro.setTipoCarro(tipoCarro);
        carro.setSubtotal(carroRequestDto.getSubtotal());
        carro.setDescuento(carroRequestDto.getDescuento());
        carro.setTotal(carroRequestDto.getTotal());
        carro.setFechaCompra(LocalDate.now());

        //buscamos el cliente para establecer la relacion bidireccional con el carro y cliente
        Cliente cliente = clienteRepository
                .findById(carroRequestDto.getCliente()).orElseThrow(() -> new NoSuchElementException("El cliente no fue encontrado."));

        cliente.getCarros().add(carro);
        carro.setCliente(cliente);

        //recorremos la lista de items
        List<CarroItem> items = new ArrayList<>();
        for (CarroItemRequestDto itemDto : carroRequestDto.getItems()) {
            CarroItem carroItem = new CarroItem();
            carroItem.setCantidad(itemDto.getCantidad());
            carroItem.setPrecioUnitario(itemDto.getPrecioUnitario());

            //buscamos el producto del item para relacionarlo
            Producto producto = productoRepository
                    .findById(itemDto.getProducto())
                    .orElseThrow(() -> new NoSuchElementException("Producto no encontrado."));

            // Establecemos la relación de los carroItem con producto y carro
            carroItem.setProducto(producto);
            carroItem.setCarro(carro);

            items.add(carroItem);
        }
        //agregamos a carro la lista de CarroItem
        carro.setItems(items);

        carro = repository.save(carro);

        //determino el año y mes, donde me fijare el valor de las compras del cliente en ese mes.
        YearMonth yearMonth = YearMonth.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue());
        //determinamos la fecha inicial que sera el primer dia del mes
        LocalDate startDate = yearMonth.atDay(1);

        //determinamos la fecha final que sera el ultimo dia del mes
        LocalDate endDate = yearMonth.atEndOfMonth();


        //BUSCAMOS LAS COMPRAS DEL CLIENTE EN EL MES ACTUAL, SI ENTRE TODAS LAS COMPRAS DEL MES SUPERA $10.000
        //SE CONVIERTE EN CLIENTE VIP
        List<Carro> carrosMesActual = repository.findAllByClienteIdAndFechaCompraBetween(cliente.getId(), startDate, endDate);
        BigDecimal totalComprasMes = BigDecimal.ZERO;
        BigDecimal valorASuperar = new BigDecimal("10.000");

        for (Carro c : carrosMesActual) {
            //sumo el total de todas las compras del mes y lo agrego en una variable para luego comparar si es superior a 10.000
            totalComprasMes = totalComprasMes.add(c.getTotal());
        }


        //validamos que no sea un cliente vip Activo y que haya superado el monto a $10.000
        if (totalComprasMes.compareTo(valorASuperar) > 0 && (cliente.isVip()==false || cliente.getFinalVip()!=null)  ) {
            log.info("El cliente ha gastado mas de 10.000 este mes. sera Vip");

            cliente.setVip(true);
            cliente.setComienzoVip(LocalDateTime.now());
            cliente.setFinalVip(null);
            //EMVIAREMOS UN EMAIL AVISANDOLE AL CLIENTE QUE ES VIP
            //USAREMOS UN HILO PARA MEJORAR LOS TIEMPOS DE RESPUESTA

            //CLASE QUE CONTIENE LA TAREA DE ENVIAR EL EMAIL
            Runnable r = new HiloEmail(cliente.getEmail(), sendMail);
            //CLASE THREAD CONTROLADORA DEL HILO
            Thread hiloemail = new Thread(r);
            hiloemail.start();

            clienteRepository.save(cliente);
        }


        CarroDto dto = CarroDtoMapper.getInstance().setCarro(carro).build();
        return dto;
    }
}
