package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carro.CarroRequestDto;
import com.juanag32.factorit.entities.Carro;

import java.util.List;

public interface CarroService {
    List<CarroDto> findAllByClient(Long idCliente);
    CarroDto findById(Long id);
    CarroDto save(CarroRequestDto carroRequestDto);
}
