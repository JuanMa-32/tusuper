package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.dtos.producto.ProductoRequestDto;

import java.util.List;

public interface ProductoService {
    ProductoDto cambiarEstado(Long id);
    List<ProductoDto> findAll();
    List<ProductoDto> findByCategoria(String categoria);

    List<ProductoDto> findByNombre(String nombre);
    List<ProductoDto> orderByPrecioAsc();
    List<ProductoDto> orderByPrecioDesc();
    List<ProductoDto> findByActivos();
    ProductoDto findById(Long id);
    ProductoDto save(ProductoRequestDto productoRequestDto);


}
