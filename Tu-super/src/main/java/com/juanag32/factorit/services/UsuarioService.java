package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.dtos.usuario.UsuarioRequestDto;

public interface UsuarioService {

    UsuarioDto findByEmail(String email);
    UsuarioDto save(UsuarioRequestDto usuarioRequestDto);
}
