package com.juanag32.factorit.services;

import com.juanag32.factorit.entities.DiaPromocion;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DiaPromocionService {

    DiaPromocion save(DiaPromocion diaPromocion);
    DiaPromocion findByFecha(LocalDate fecha);
}
