package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.dtos.producto.ProductoRequestDto;
import com.juanag32.factorit.entities.Producto;
import com.juanag32.factorit.enums.Categoria;
import com.juanag32.factorit.enums.TipoCarro;
import com.juanag32.factorit.enums.UnidadMedida;
import com.juanag32.factorit.mappers.ProductoDtoMapper;
import com.juanag32.factorit.repositories.ProductoRepository;
import com.juanag32.factorit.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductoServiceImpl implements ProductoService {
    @Autowired
    private ProductoRepository repository;

    @Override
    public ProductoDto cambiarEstado(Long id) {
        Producto producto = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Producto no encontrado"));

        if(producto.isActivo()){
            producto.setActivo(false);
        }else{
            producto.setActivo(true);
        }

        producto = repository.save(producto);
        ProductoDto dto = ProductoDtoMapper.getInstance().setProducto(producto).build();
        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> findAll() {
        List<ProductoDto> productoDtos = repository.findAll().stream().map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> findByCategoria(String categoria) {
        Categoria nombreCategoria = Categoria.valueOf(categoria.toUpperCase());
        List<ProductoDto> productoDtos = repository.findByCategoriaAndActivoTrue(nombreCategoria).stream()
                .map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> findByNombre(String nombre) {

        List<ProductoDto> productoDtos = repository.findByNombreIgnoreCaseContainingAndActivoTrue(nombre).stream().
                map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> orderByPrecioAsc() {
        List<ProductoDto> productoDtos = repository.findAllByActivoTrueOrderByPrecioAsc().stream()
                .map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> orderByPrecioDesc() {
        List<ProductoDto> productoDtos = repository.findAllByActivoTrueOrderByPrecioDesc().stream()
                .map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductoDto> findByActivos() {
        List<ProductoDto> productoDtos = repository.findAllByActivoTrue().stream().map(p -> ProductoDtoMapper.getInstance().setProducto(p).build()).collect(Collectors.toList());
        return productoDtos;
    }

    @Override
    @Transactional(readOnly = true)
    public ProductoDto findById(Long id) {
        Producto producto = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Producto no encontrado"));
        ProductoDto dto = ProductoDtoMapper.getInstance().setProducto(producto).build();
        return dto;
    }

    @Override
    @Transactional
    public ProductoDto save(ProductoRequestDto productoRequestDto) {
        Producto producto = new Producto();
        producto.setNombre(productoRequestDto.getNombre());
        producto.setPrecio(productoRequestDto.getPrecio());

        Categoria nombreCategoria = Categoria.valueOf(productoRequestDto.getCategoria().toUpperCase());
        producto.setCategoria(nombreCategoria);
        producto.setActivo(true);
        producto.setMedidaValor(productoRequestDto.getMedidaValor());
        UnidadMedida unidadMedida = UnidadMedida.valueOf(productoRequestDto.getUnidadMedida().toUpperCase());
        producto.setUnidadMedida(unidadMedida);

        producto = repository.save(producto);

        ProductoDto dto = ProductoDtoMapper.getInstance().setProducto(producto).build();
        return dto;
    }
}
