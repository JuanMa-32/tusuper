package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.dtos.usuario.UsuarioRequestDto;
import com.juanag32.factorit.entities.Usuario;
import com.juanag32.factorit.enums.Role;
import com.juanag32.factorit.mappers.UsuarioDtoMapper;
import com.juanag32.factorit.repositories.UsuarioRepository;
import com.juanag32.factorit.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
public class UsuarioServiceImpl implements UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Override
    @Transactional(readOnly = true)
    public UsuarioDto findByEmail(String email) {
        Usuario us = repository.findByEmail(email).orElseThrow(()-> new NoSuchElementException("Usuario no encontrado."));
        UsuarioDto dto = UsuarioDtoMapper.getInstance().setUsuario(us).build();
        return dto;
    }

    @Override
    @Transactional
    public UsuarioDto save(UsuarioRequestDto usuarioRequestDto) {
        Usuario usuario = new Usuario();
        usuario.setNombre(usuarioRequestDto.getNombre());
        usuario.setApellido(usuarioRequestDto.getApellido());
        usuario.setEmail(usuarioRequestDto.getEmail());
        usuario.setPassword(usuarioRequestDto.getPassword());
        usuario.setRole(Role.ADMIN);

        usuario = repository.save(usuario);
        UsuarioDto dto = UsuarioDtoMapper.getInstance().setUsuario(usuario).build();
        return dto;
    }
}
