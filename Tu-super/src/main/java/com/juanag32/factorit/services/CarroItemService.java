package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.carroItem.CarroItemDto;

public interface CarroItemService {
    CarroItemDto findById(Long id);

}
