package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.repositories.CarroItemRepository;
import com.juanag32.factorit.services.CarroItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CarroItemServiceImpl implements CarroItemService {

    /* ESTE SERVICES NO TUVO QUE SER USADO YA QUE EN LA ENTIDAD CARRO USO UN CASCADE ALL
    * EN LA RELACION CON CARROITEM Y SE ENCARGA DE LAS TAREAS DE PERSISTENCIA .
    * */
    @Autowired
    CarroItemRepository repository;
    @Override
    @Transactional(readOnly = true)
    public CarroItemDto findById(Long id) {
        return null;
    }
}
