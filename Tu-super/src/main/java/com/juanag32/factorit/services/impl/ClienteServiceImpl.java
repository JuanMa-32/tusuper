package com.juanag32.factorit.services.impl;

import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.dtos.cliente.ClienteRequestDto;
import com.juanag32.factorit.entities.Cliente;
import com.juanag32.factorit.enums.Role;
import com.juanag32.factorit.mappers.ClienteDtoMapper;
import com.juanag32.factorit.repositories.ClienteRepository;
import com.juanag32.factorit.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = true)
    public List<ClienteDto> findAll() {
        List<Cliente> clientes = repository.findAll();
        //convierto todos los Clientes en ClientesDto usando la clase ClienteDtoMapper para transferir los datos necesarios.
        List<ClienteDto> clientesDto = clientes.stream().map(c -> ClienteDtoMapper.getInstance().setCliente(c).build()).collect(Collectors.toList());
        return clientesDto;
    }

    @Override
    public ClienteDto cambiarEstadoVip(Long id) {
        Cliente client = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Cliente no encontrado"));
        if(client.isVip()){
            client.setFinalVip(LocalDateTime.now());
            client= repository.save(client);
        }
        ClienteDto dto = ClienteDtoMapper.getInstance().setCliente(client).build();
        return dto;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClienteDto> clientesVip() {
        List<ClienteDto> clientes = repository.findAllByVipTrue().stream().map(c->ClienteDtoMapper.getInstance().setCliente(c).build()).collect(Collectors.toList());
        return clientes;
    }

    @Override
    @Transactional(readOnly = true)
    public ClienteDto findById(Long id) {
        Cliente client = repository.findById(id).orElseThrow(() -> new NoSuchElementException("Cliente no encontrado"));
        ClienteDto dto = ClienteDtoMapper.getInstance().setCliente(client).build();
        return dto;
    }

    @Override
    @Transactional
    public ClienteDto save(ClienteRequestDto clienteRequestDto) {
        Cliente cliente = new Cliente();
        cliente.setNombre(clienteRequestDto.getNombre());
        cliente.setApellido(clienteRequestDto.getApellido());
        cliente.setEmail(clienteRequestDto.getEmail());
        cliente.setPassword(passwordEncoder.encode(clienteRequestDto.getPassword()));
        cliente.setRole(Role.CLIENT);
        cliente = repository.save(cliente);

        ClienteDto dto = ClienteDtoMapper.getInstance().setCliente(cliente).build();
        return dto;
    }


}
