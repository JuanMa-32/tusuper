package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.dtos.cliente.ClienteRequestDto;

import java.util.List;
import java.util.Optional;

public interface ClienteService {

    List<ClienteDto> findAll();
    ClienteDto cambiarEstadoVip(Long id);
    List<ClienteDto>clientesVip();
    ClienteDto findById(Long id);
    ClienteDto save(ClienteRequestDto clienteRequestDto);
}
