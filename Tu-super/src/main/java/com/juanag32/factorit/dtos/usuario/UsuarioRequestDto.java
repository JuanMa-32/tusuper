package com.juanag32.factorit.dtos.usuario;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioRequestDto {
    @NotBlank(message = "campo nombre es obligatorio.")
    private String nombre;
    @NotBlank(message = "campo nombre es obligatorio.")
    private String apellido;
    @NotBlank(message = "campo nombre es obligatorio.")
    private String email;
    @NotBlank(message = "campo nombre es obligatorio.")
    private String password;
}
