package com.juanag32.factorit.dtos.producto;

import com.juanag32.factorit.enums.UnidadMedida;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class ProductoDto {


    private Long id;
    private String nombre;
    private BigDecimal precio;

    private String categoria;

    private String unidadMedida;
    private Integer medidaValor;
    private boolean activo;
}
