package com.juanag32.factorit.dtos.producto;

import com.juanag32.factorit.enums.UnidadMedida;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductoRequestDto {
    @NotBlank(message = "Campo nombre es obligatorio.")
    private String nombre;
    @NotNull(message = "Campo precio es obligatorio.")
    private BigDecimal precio;

    @NotBlank(message = "Campo categoria es obligatorio.")
    private String categoria;

    @NotBlank(message = "Campo unidad Medida es obligatorio.")
    private String unidadMedida;
    @NotNull(message = "Campo medida Valor es obligatorio.")
    private int medidaValor;

}
