package com.juanag32.factorit.dtos.carroItem;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarroItemRequestDto {
    @NotNull(message = "campo producto obligatorio.")
    private Long producto;
    @NotNull(message = "campo cantidad obligatorio.")
    private Integer cantidad;
    @NotNull(message = "campo precio unitario obligatorio.")
    private BigDecimal precioUnitario;
}
