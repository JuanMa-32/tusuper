package com.juanag32.factorit.dtos.cliente;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ClienteDto {
    private Long id;
    private String nombre;
    private String apellido;
    private String email;
    //private List<CarroDto> carros;
    private boolean vip;
    private LocalDateTime comienzoVip;
    private LocalDateTime finalVip;

}
