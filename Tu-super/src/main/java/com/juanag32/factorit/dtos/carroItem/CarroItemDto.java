package com.juanag32.factorit.dtos.carroItem;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
public class CarroItemDto {
    private Long id;
    private ProductoDto producto;
    private int cantidad;
    private BigDecimal precioUnitario;
}
