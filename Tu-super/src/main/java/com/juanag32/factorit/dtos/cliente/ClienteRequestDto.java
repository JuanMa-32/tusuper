package com.juanag32.factorit.dtos.cliente;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClienteRequestDto {
    @NotBlank(message = "Campo nombre es obligatorio.")
    private String nombre;
    @NotBlank(message = "Campo apellido es obligatorio.")
    private String apellido;
    @Email(message = "Formato incorrecto.")
    private String email;
    @NotBlank(message = "Campo password es obligatorio.")
    @Size(min = 6,message = "El password debe tener al menos 6 digitos.")
    private String password;
}
