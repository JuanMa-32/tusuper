package com.juanag32.factorit.dtos.carro;

import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.entities.CarroItem;
import com.juanag32.factorit.enums.TipoCarro;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor

public class CarroDto {
    private Long id;
    private LocalDate fechaCompra;
    private BigDecimal subtotal;
    private BigDecimal descuento;
    private BigDecimal total;
    private String tipoCarro;
    private List<CarroItemDto> items;

}
