package com.juanag32.factorit.dtos.carro;

import com.juanag32.factorit.dtos.carroItem.CarroItemRequestDto;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarroRequestDto {
    @NotNull(message = "Campo cliente obligatorio.")
    private Long cliente;

    @NotNull(message = "Campo subTotal obligatorio.")
    private BigDecimal subtotal;
    private BigDecimal descuento;
    @NotNull(message = "Campo total obligatorio.")
    private BigDecimal total;

    @NotBlank(message = "Campo tipo de carro obligatorio.")
    private String tipoCarro;
    @Valid
    @NotNull(message = "Campo items obligatorio.")
    private List<CarroItemRequestDto> items;
}
