package com.juanag32.factorit.controllers;

import com.juanag32.factorit.entities.DiaPromocion;
import com.juanag32.factorit.services.DiaPromocionService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@RestController
@RequestMapping("/api/dia-promocion")
@CrossOrigin("*")
public class DiaPromocionController {

    @Autowired
    private DiaPromocionService service;

    @PostMapping("/save")
    public ResponseEntity<?> guardarPromocion(@Valid @RequestBody DiaPromocion diaPromocion){
        DiaPromocion promocionDb = service.save(diaPromocion);
        return ResponseEntity.status(201).body(promocionDb);
    }

    @GetMapping("/buscar-promocion")
    public ResponseEntity<?> buscarDiaPromocionPorFecha(@RequestParam String fecha) {
        LocalDate localDate = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy/MM/dd"));
            DiaPromocion diaPromocion = service.findByFecha(localDate);
            return ResponseEntity.ok(diaPromocion);
    }
}
