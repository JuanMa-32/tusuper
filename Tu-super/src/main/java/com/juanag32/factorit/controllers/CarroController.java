package com.juanag32.factorit.controllers;

import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carro.CarroRequestDto;
import com.juanag32.factorit.services.CarroService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/carro")
@CrossOrigin("*")
public class CarroController {
    @Autowired
    private CarroService service;

    @PostMapping("/save")
    public ResponseEntity<?> saveCarro(@Valid @RequestBody CarroRequestDto carro){
        CarroDto carroDto = service.save(carro);
        return ResponseEntity.status(201).body(carroDto);
    }
    @GetMapping("/carros/client/{idCliente}")
    public ResponseEntity<?> findAllByClienteId(@PathVariable Long idCliente){
        List<CarroDto> carrosDto = service.findAllByClient(idCliente);
        return ResponseEntity.ok(carrosDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        CarroDto dto = service.findById(id);
        return ResponseEntity.ok(dto);
    }
}
