package com.juanag32.factorit.controllers;

import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.dtos.usuario.UsuarioRequestDto;
import com.juanag32.factorit.services.UsuarioService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/usuarios")
@CrossOrigin(origins = "*")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @PostMapping("/save")
    public ResponseEntity<?> saveUsuario(@Valid @RequestBody UsuarioRequestDto usuario){
        UsuarioDto dto = service.save(usuario);
        return ResponseEntity.status(201).body(dto);
    }


}
