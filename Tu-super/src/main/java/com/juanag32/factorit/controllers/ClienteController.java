package com.juanag32.factorit.controllers;

import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.dtos.cliente.ClienteRequestDto;
import com.juanag32.factorit.services.ClienteService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/clientes")
@CrossOrigin("*")
public class ClienteController {
    @Autowired
    private ClienteService service;

    @PostMapping("/save")
    public ResponseEntity<?> saveCliente(@Valid @RequestBody ClienteRequestDto cliente) {
        ClienteDto clienteDto = service.save(cliente);
        return ResponseEntity.status(201).body(clienteDto);
    }

    @PutMapping("/cancelar-vip/{id}")
    public ResponseEntity<?> cancelarVip(@PathVariable Long id) {
        ClienteDto clienteDtos = service.cambiarEstadoVip(id);
        return ResponseEntity.ok(clienteDtos);
    }

    @GetMapping("/clientes-vip")
    public ResponseEntity<?> clientesVip() {
        List<ClienteDto> clienteDtos = service.clientesVip();
        return ResponseEntity.ok(clienteDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        ClienteDto clienteDtos = service.findById(id);
        return ResponseEntity.ok(clienteDtos);
    }

}
