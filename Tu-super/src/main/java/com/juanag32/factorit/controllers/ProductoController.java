package com.juanag32.factorit.controllers;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.dtos.producto.ProductoRequestDto;
import com.juanag32.factorit.services.ProductoService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/productos")
@CrossOrigin(origins = "*")
public class ProductoController {

    @Autowired
    ProductoService service;
    @PostMapping("/save")
    public ResponseEntity<?> saveProducto(@Valid @RequestBody ProductoRequestDto producto){
        ProductoDto dto = service.save(producto);
        return ResponseEntity.status(201).body(dto);
    }
    @PutMapping("/cambiar-estado/{id}")
    public ResponseEntity<?> cambiarEstadoroducto(@PathVariable Long id){
        ProductoDto dto = service.cambiarEstado(id);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/activos")
    public ResponseEntity<?> findAllByActivos(){
        List<ProductoDto> productoDtos = service.findByActivos();
        return ResponseEntity.ok(productoDtos);
    }
    @GetMapping("/buscar-categoria/{categoria}")
    public ResponseEntity<?> findByCategoria(@PathVariable String categoria){
        List<ProductoDto> productoDtos = service.findByCategoria(categoria);
        return ResponseEntity.ok(productoDtos);
    }
    @GetMapping("/precio-asc")
    public ResponseEntity<?> orderByPrecioAsc(){
        List<ProductoDto> productoDtos = service.orderByPrecioAsc();
        return ResponseEntity.ok(productoDtos);
    }
    @GetMapping("/precio-desc")
    public ResponseEntity<?> orderByPrecioDesc(){
        List<ProductoDto> productoDtos = service.orderByPrecioDesc();
        return ResponseEntity.ok(productoDtos);
    }

    @GetMapping("/{nombre}")
    public ResponseEntity<?> buscarPorNombre(@PathVariable String nombre){
        List<ProductoDto> dto = service.findByNombre(nombre);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/all")
    public ResponseEntity<?> findAll(){
        List<ProductoDto> productoDtos = service.findAll();
        return ResponseEntity.ok(productoDtos);
    }
}
