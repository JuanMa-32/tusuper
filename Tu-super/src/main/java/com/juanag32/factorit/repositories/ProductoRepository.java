package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Producto;
import com.juanag32.factorit.enums.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductoRepository extends JpaRepository<Producto,Long> {

    List<Producto> findByNombreIgnoreCaseContainingAndActivoTrue(String nombre);
    List<Producto> findAllByActivoTrueOrderByPrecioAsc();
    List<Producto> findAllByActivoTrueOrderByPrecioDesc();
    List<Producto> findAllByActivoTrue();
    List<Producto> findByCategoriaAndActivoTrue(Categoria categoria);

}
