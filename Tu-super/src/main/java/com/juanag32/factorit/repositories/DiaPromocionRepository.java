package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.DiaPromocion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public interface DiaPromocionRepository extends JpaRepository<DiaPromocion,Long> {
    Optional<DiaPromocion> findByFecha(LocalDate fecha);
}
