package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente,Long> {
    List<Cliente> findAllByVipTrue();
}
