package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Carro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CarroRepository extends JpaRepository<Carro,Long> {
    List<Carro> findAllByClienteId(Long id);
    List<Carro> findAllByClienteIdAndFechaCompraBetween(Long clienteId, LocalDate startDate, LocalDate endDate);
}
