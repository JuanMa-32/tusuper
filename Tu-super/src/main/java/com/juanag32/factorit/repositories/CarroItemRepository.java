package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.CarroItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarroItemRepository extends JpaRepository<CarroItem,Long> {
}
