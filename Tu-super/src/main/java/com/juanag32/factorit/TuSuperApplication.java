package com.juanag32.factorit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class TuSuperApplication {

	public static void main(String[] args) {
		SpringApplication.run(TuSuperApplication.class, args);

		String password = "contraseña123";
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(password);
		System.out.println("Contraseña : " + encodedPassword);
	}

}
