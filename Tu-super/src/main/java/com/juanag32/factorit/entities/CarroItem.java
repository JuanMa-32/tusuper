package com.juanag32.factorit.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor
@Table(name = "carro_items")
public class CarroItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "producto_id")
    private Producto producto;

    private Integer cantidad;
    @Column(name = "precio_unitario")
    private BigDecimal precioUnitario;
    @ManyToOne
    @JoinColumn(name = "carro_id")
    private Carro carro;
}
