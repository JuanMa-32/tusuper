package com.juanag32.factorit.entities;

import com.juanag32.factorit.enums.TipoCarro;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor@NoArgsConstructor
@Table(name = "carros")
public class Carro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;
    @Column(name = "fecha_compra")
    private LocalDate fechaCompra;
    private BigDecimal subtotal;
    private BigDecimal descuento;
    private BigDecimal total;

    @Enumerated(EnumType.STRING)
    private TipoCarro tipoCarro;

    @OneToMany(mappedBy = "carro",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<CarroItem> items = new ArrayList<>();

}
