package com.juanag32.factorit.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor@NoArgsConstructor
@PrimaryKeyJoinColumn(name="client_id")
public class Cliente extends Usuario {


    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
    private List<Carro> carros= new ArrayList<>();
    private boolean vip;
    private LocalDateTime comienzoVip;
    private LocalDateTime finalVip;

}
