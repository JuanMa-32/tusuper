package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.DiaPromocion;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@DataJpaTest
public class DiaPromocionRepositoryTest {
    @Autowired
    DiaPromocionRepository repository;

    @Nested
    class testsRead{
        @Test
        void testFindByFecha() {
            //given
            LocalDate fecha = LocalDate.of(2024,12,25);
            //when
            Optional<DiaPromocion> o = repository.findByFecha(fecha);
            //then
            assertTrue(o.isPresent());
            assertEquals(new BigDecimal("300.00"),o.orElseThrow().getDescuento());
        }
    }

    @Test
    void testSaveDiaPromocion() {
        //given
        DiaPromocion diaPromocion=null;
        diaPromocion = new DiaPromocion();
        diaPromocion.setDescuento(new BigDecimal("100"));
        diaPromocion.setFecha(LocalDate.now());
        //when
        diaPromocion = repository.save(diaPromocion);
        //then
        assertNotNull(diaPromocion.getId());
        assertEquals(6,diaPromocion.getFecha().getMonthValue());
    }
}
