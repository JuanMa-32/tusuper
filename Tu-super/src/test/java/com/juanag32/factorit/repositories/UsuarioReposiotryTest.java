package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Cliente;
import com.juanag32.factorit.entities.Usuario;

import static org.junit.jupiter.api.Assertions.*;

import com.juanag32.factorit.enums.Role;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

@DataJpaTest
public class UsuarioReposiotryTest {
    @Autowired
    private UsuarioRepository repository;

    @Nested
    class testsRead {
        @Test
        void testFindByEmail() {
            //given
            String email= "juan@example.com";
            //when
            Optional<Usuario> us = repository.findByEmail(email);
            //then
            assertFalse(us.isEmpty());
            assertEquals(email,us.orElseThrow().getEmail());
        }

        @Test
        void testFindAll() {
            //when
            List<Usuario> usuarios = repository.findAll();
            //then
            Usuario usuario = usuarios.get(2);
            boolean esCliente = usuario instanceof Cliente;
            assertTrue(esCliente);
            assertFalse(usuarios.isEmpty());
            assertEquals(3, usuarios.size());
        }

        @Test
        void testfindById() {
            //GIVEN
            Optional<Usuario> us = repository.findById(5L);

            //then
            assertTrue(us.isPresent());
            assertEquals("Juan", us.orElseThrow().getNombre());
        }

    }
    @Nested
    class testsGuardado{
        @Test
        void testSaveUsuario() {
            //given
            Usuario us = null;
             us = new Usuario();
            us.setNombre("Leonel");
            us.setApellido("Messi");
            us.setRole(Role.ADMIN);
            us.setEmail("messi@messi.com");
            //when
            us = repository.save(us);

            //then
            assertNotNull(us.getId());
            assertEquals("Leonel",us.getNombre());

        }

    }
}
