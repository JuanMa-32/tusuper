package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Carro;
import com.juanag32.factorit.entities.CarroItem;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@DataJpaTest
public class CarroRepositoryTest {
    @Autowired
    CarroRepository repository;

    @Nested
    class testsRead {

        @Test
        void testCarrosClienteEnUnMesDeterminado() {
            YearMonth yearMonth = YearMonth.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue());
            LocalDate startDate = yearMonth.atDay(1);
            LocalDate endDate = yearMonth.atEndOfMonth();

            List<Carro> carrosMesActual=repository.findAllByClienteIdAndFechaCompraBetween(6L,startDate,endDate);

            System.out.println(carrosMesActual.get(0).getFechaCompra());

            assertFalse(carrosMesActual.isEmpty());
            assertEquals(6,carrosMesActual.get(0).getFechaCompra().getMonthValue());
        }

        @Test
        void testCarrosCliente() {
            List<Carro> carrosCliente= repository.findAllByClienteId(6L);
            assertFalse(carrosCliente.isEmpty());
            assertEquals("Maria",carrosCliente.get(0).getCliente().getNombre());
        }

        @Test
        void testFindById() {
            //when
            Optional<Carro> o = repository.findById(6L);
            //then
            assertTrue(o.isPresent());
            assertEquals("Carlos", o.orElseThrow().getCliente().getNombre());
            assertFalse(o.orElseThrow().getItems().isEmpty());
            assertEquals("Arroz", o.orElseThrow().getItems().get(0).getProducto().getNombre());
        }

        @Test
        void testFindAll() {
            //when
            List<Carro> carros = repository.findAll();
            //then
            assertFalse(carros.isEmpty());
        }
    }

    @Test
    void testGuardarCarroYsusItems() {
        //given
        CarroItem carroItem = new CarroItem();
        carroItem.setCantidad(2);
        carroItem.setPrecioUnitario(new BigDecimal("1200"));
        CarroItem carroItem2 = new CarroItem();

        Carro carro = null;
        carro = new Carro();
        carro.setItems(Arrays.asList(carroItem2, carroItem));
        carro.setFechaCompra(LocalDate.now());

        carroItem.setCarro(carro);
        carroItem2.setCarro(carro);
        //when
        carro = repository.save(carro);

        //then
        assertNotNull(carro.getId());
        assertFalse(carro.getItems().isEmpty());
        assertEquals(1,carro.getItems().get(0).getId());
        assertNotNull(carro.getItems().get(0).getCarro());
        assertNotNull(carro.getItems().get(0).getId());
    }
}
