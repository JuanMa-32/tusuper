package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Cliente;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    ClienteRepository repository;

    @Nested
    class testRead {
        @Test
        void testClientesVip() {
            List<Cliente> clientes = repository.findAllByVipTrue();

            assertFalse(clientes.isEmpty());

        }

        @Test
        void testFindAll() {
           // when
             List<Cliente> clientes = repository.findAll();
           // then
            assertFalse(clientes.isEmpty());
            assertNotNull(clientes.get(0).getId());
            assertEquals(2,clientes.size());
        }

        @Test
        void testFindById() {
            //when
            Optional<Cliente> o = repository.findById(6L);

            //then
            assertTrue(o.isPresent());
            assertEquals(6,o.orElseThrow().getId());
        }
    }

    @Nested
    class testsSave {
        @Test
        void testSaveCliente() {
            //given
            Cliente cliente = null;
            cliente = new Cliente();
            cliente.setNombre("Rosalia");
            cliente.setVip(true);
            //when
            cliente = repository.save(cliente);
            //then
            assertNotNull(cliente.getId());
            assertEquals("Rosalia",cliente.getNombre());

        }
    }
}
