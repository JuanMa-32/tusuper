package com.juanag32.factorit.repositories;

import com.juanag32.factorit.entities.Producto;
import static org.junit.jupiter.api.Assertions.*;

import com.juanag32.factorit.enums.Categoria;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@DataJpaTest
public class ProductoRepositoryTest {

    @Autowired
    ProductoRepository repository;

    @Nested
    class testsRead{
        @Test
        void testFindByCategoria() {
            String nombreCat = "bebidas";
            Categoria categoria = Categoria.valueOf(nombreCat.toUpperCase());
            List<Producto> productos = repository.findByCategoriaAndActivoTrue(categoria);

            //then
            assertFalse(productos.isEmpty());
        }

        @Test
        void testFindActivos() {
            //when
            List<Producto> productos= repository.findAllByActivoTrue();

            //then
            assertFalse(productos.isEmpty());
        }

        @Test
        void testOrderByCaro() {

            //when
            List<Producto> productos = repository.findAllByActivoTrueOrderByPrecioDesc();
            //then
            assertEquals(new BigDecimal("1655.00"),productos.get(0).getPrecio());
        }
        @Test
        void testOrderByBarato() {

            //when
            List<Producto> productos = repository.findAllByActivoTrueOrderByPrecioAsc();
            //then
            assertEquals(new BigDecimal("152.33"),productos.get(0).getPrecio());
        }

        @Test
        void testFindByNombreContaining() {
            //given
            String nombre= "lec";
            //when
            List<Producto> productos = repository.findByNombreIgnoreCaseContainingAndActivoTrue(nombre);
            //then
            assertFalse(productos.isEmpty());
            assertEquals("Leche",productos.get(0).getNombre());
        }

        @Test
        void testFindAll() {
            //when
            List<Producto> productos = repository.findAll();
            //then
            assertFalse(productos.isEmpty());
            assertEquals("Manzana",productos.get(0).getNombre());
            assertEquals("KILOGRAMO",productos.get(0).getUnidadMedida().toString());
        }

        @Test
        void testFindById() {
            //when
            Optional<Producto> o = repository.findById(7L);
            //then
            assertTrue(o.isPresent());
            assertEquals("Arroz",o.orElseThrow().getNombre());
        }
    }

    @Test
    void testSaveProducto() {
        //given
        Producto producto=null;
        producto = new Producto();
        producto.setNombre("Banana");
        producto.setPrecio(new BigDecimal("320"));

        //when
        producto = repository.save(producto);
        //then
        assertNotNull(producto.getId());
        assertEquals(new BigDecimal("320"),producto.getPrecio());
    }
}
