package com.juanag32.factorit.entities;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class DiaPromocionTest {

    @Test
    void testInstanciarPromocion() {
        //given
        DiaPromocion diaPromocion = new DiaPromocion();
        diaPromocion.setFecha(LocalDate.now());
        diaPromocion.setDescuento(new BigDecimal("300.00"));

        //then
        assertEquals(LocalDateTime.now().getDayOfMonth(),diaPromocion.getFecha().getDayOfMonth());
        assertEquals(new BigDecimal("300.00"),diaPromocion.getDescuento());
    }
}
