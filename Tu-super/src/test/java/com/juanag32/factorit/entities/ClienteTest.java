package com.juanag32.factorit.entities;

import com.juanag32.factorit.enums.Role;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class ClienteTest {

    @Test
    void testInstanciarCliente() {
        //given
        Cliente cliente = new Cliente();
        cliente.setNombre("India");
        cliente.setApellido("Agüero");
        cliente.setEmail("india22@gmailc.com");
        cliente.setRole(Role.CLIENT);
        cliente.setCarros(Arrays.asList(new Carro(),new Carro()));

        //then
        assertEquals("India",cliente.getNombre());
        assertEquals("CLIENT",cliente.getRole().toString());
        assertFalse(cliente.getCarros().isEmpty());
    }
}
