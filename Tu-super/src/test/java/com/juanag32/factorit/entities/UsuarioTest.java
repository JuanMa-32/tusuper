package com.juanag32.factorit.entities;

import com.juanag32.factorit.enums.Role;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class UsuarioTest {


    @Test
    void testInstanciarUsuario() {
        //given
        Usuario usuario = new Usuario();
        usuario.setNombre("Juan Manuel");
        usuario.setApellido("Agüero");
        usuario.setEmail("juanma4@gmailc.com");
        usuario.setRole(Role.ADMIN);

        //then
        assertEquals("Juan Manuel",usuario.getNombre());
        assertEquals("ADMIN",usuario.getRole().toString());
    }
}
