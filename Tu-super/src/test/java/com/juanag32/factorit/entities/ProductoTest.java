package com.juanag32.factorit.entities;

import static org.junit.jupiter.api.Assertions.*;

import com.juanag32.factorit.enums.UnidadMedida;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductoTest {

    private Producto producto;

    @BeforeEach
    void setUp() {
        //given
         producto = new Producto();
        producto.setNombre("Gaseosa Coca cola");
        producto.setMedidaValor(1);
        producto.setUnidadMedida(UnidadMedida.LITRO);

    }

    @Test
    void testInstanciarProducto() {

        //then
        assertEquals("Gaseosa Coca cola",producto.getNombre());
        assertEquals(1,producto.getMedidaValor());
        assertEquals("LITRO",producto.getUnidadMedida().toString());
    }

}
