package com.juanag32.factorit.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CarroItemTest {

    @Test
    void testInstanciarCarroItem() {
        //given
        CarroItem carroItem = new CarroItem();
        Producto p = new Producto();
        p.setNombre("Gaseosa Coca Cola");
        carroItem.setProducto(p);
        carroItem.setCantidad(2);
        carroItem.setPrecioUnitario(new BigDecimal("123.23"));

        //then
        assertNotNull(carroItem.getProducto());
        assertEquals("Gaseosa Coca Cola", carroItem.getProducto().getNombre());
        assertTrue(carroItem.getCantidad() > 0);
        assertEquals(new BigDecimal("123.23"),carroItem.getPrecioUnitario());

    }
}
