package com.juanag32.factorit.entities;

import static org.junit.jupiter.api.Assertions.*;

import com.juanag32.factorit.enums.TipoCarro;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class CarroTest {

    @Test
    void testInstanciarCarro() {
        //given
        Carro carro = new Carro();
        Cliente cliente = new Cliente();
        cliente.setNombre("Dario");
        carro.setCliente(cliente);
        carro.setFechaCompra(LocalDate.now());
        carro.setTipoCarro(TipoCarro.VIP);
        carro.setTotal(new BigDecimal("352.333"));
        CarroItem carroItem1 = new CarroItem();
        Producto p1 = new Producto();
        p1.setNombre("Gaseosa Coca Cola");
        carroItem1.setProducto(p1);
        carro.setItems(Arrays.asList(carroItem1));

        //then
        assertFalse(carro.getItems().isEmpty());
        assertEquals("Gaseosa Coca Cola",carro.getItems().get(0).getProducto().getNombre());
    }
}
