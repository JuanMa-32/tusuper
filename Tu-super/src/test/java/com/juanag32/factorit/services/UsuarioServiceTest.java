package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.dtos.usuario.UsuarioRequestDto;
import com.juanag32.factorit.entities.Usuario;
import com.juanag32.factorit.enums.Role;
import com.juanag32.factorit.repositories.UsuarioRepository;
import com.juanag32.factorit.services.impl.UsuarioServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UsuarioServiceTest {

    @Mock
    UsuarioRepository repository;
    @InjectMocks
    UsuarioServiceImpl service;

    Usuario usuario;

    @BeforeEach
    void setUp() {
        usuario = new Usuario();
        usuario.setId(1L);
        usuario.setNombre("Juan Manuel");
        usuario.setApellido("Agüero");
        usuario.setEmail("juanma4@gmail.com");
        usuario.setPassword("12313");
        usuario.setRole(Role.ADMIN);
    }

    @Test
    void testFindByEmail() {
        when(repository.findByEmail(anyString())).thenReturn(Optional.of(usuario));

        UsuarioDto dto = service.findByEmail("juanma4@gmail.com");
        assertEquals(1, dto.getId());
        assertEquals("ADMIN", dto.getRol());

        verify(repository).findByEmail(anyString());
    }

    @Test
    void testSaveUsuario() {
        UsuarioRequestDto usuarioRequestDto = new UsuarioRequestDto("Juan Manuel", "Agüero", "juanma4@gmail.com", "12313");

        when(repository.save(any(Usuario.class))).thenReturn(usuario);

        UsuarioDto dto = service.save(usuarioRequestDto);
        assertEquals(1, dto.getId());
        assertEquals("ADMIN", dto.getRol());

        verify(repository).save(any(Usuario.class));

    }
}
