package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carro.CarroRequestDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemRequestDto;
import com.juanag32.factorit.entities.Carro;
import com.juanag32.factorit.entities.CarroItem;
import com.juanag32.factorit.entities.Cliente;
import com.juanag32.factorit.entities.Producto;
import com.juanag32.factorit.enums.Categoria;
import com.juanag32.factorit.enums.TipoCarro;
import com.juanag32.factorit.repositories.CarroRepository;
import com.juanag32.factorit.repositories.ClienteRepository;
import com.juanag32.factorit.repositories.ProductoRepository;
import com.juanag32.factorit.services.impl.CarroServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CarroServiceTest {

    @Mock
    CarroRepository repository;
    @Mock
    ProductoRepository productoRepository;
    @Mock
    ClienteRepository clienteRepository;
    @InjectMocks
    CarroServiceImpl service;
    Carro carro;

    @BeforeEach
    void setUp() {
        carro = new Carro();
        Cliente cliente = new Cliente();
        cliente.setId(1L);
        carro.setId(1L);
        carro.setFechaCompra(LocalDate.now());
        carro.setSubtotal(new BigDecimal("500"));
        carro.setTotal(new BigDecimal("500"));
        carro.setTipoCarro(TipoCarro.COMUN);
        CarroItem carroItem = new CarroItem();
        Producto producto = new Producto();
        producto.setId(1l);
        producto.setCategoria(Categoria.BEBIDAS);

        carroItem.setProducto(producto);
        carroItem.setCantidad(3);
        carro.setItems(Arrays.asList(carroItem));
    }

    @Test
    void findByIdSuccess() {
        when(repository.findById(anyLong())).thenReturn(Optional.of(carro));
        CarroDto carroDto = service.findById(1L);

        assertNotNull(carroDto.getId());
        assertEquals("COMUN", carroDto.getTipoCarro());

        verify(repository).findById(anyLong());
    }

    @Test
    void findAllByCliente() {
        when(repository.findAllByClienteId(anyLong())).thenReturn(Arrays.asList(carro));
        List<CarroDto> carroDto = service.findAllByClient(1L);

        assertFalse(carroDto.isEmpty());
        assertEquals(1,carroDto.get(0).getId());

        verify(repository).findAllByClienteId(anyLong());
    }

    @Test
    void testSaveCarro() {
        CarroRequestDto carroRequestDto= new CarroRequestDto();
        carroRequestDto.setCliente(1L);
        carroRequestDto.setTipoCarro("vip");
        carroRequestDto.setSubtotal(new BigDecimal("1455"));
        carroRequestDto.setTotal(new BigDecimal("1455"));


        CarroItemRequestDto carroItemRequestDto = new CarroItemRequestDto(1L,3,new BigDecimal("3500"));
        carroRequestDto.setItems(Arrays.asList(carroItemRequestDto));

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNombre("Juan");

        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("Gaseosa Coca Cola");
        producto.setPrecio(new BigDecimal("356"));
        producto.setCategoria(Categoria.BEBIDAS);

        when(clienteRepository.findById(anyLong())).thenReturn(Optional.of(cliente));
        when(productoRepository.findById(anyLong())).thenReturn(Optional.of(producto));
        when(repository.save(any(Carro.class))).thenReturn(carro);

        CarroDto carroDto = service.save(carroRequestDto);

        assertEquals(3,carroDto.getItems().get(0).getCantidad());
        assertNotNull(carroDto.getId());

        verify(repository).save(any(Carro.class));
        verify(clienteRepository).findById(anyLong());
        verify(productoRepository).findById(anyLong());


    }
}
