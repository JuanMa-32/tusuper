package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.dtos.cliente.ClienteRequestDto;
import com.juanag32.factorit.entities.Cliente;
import com.juanag32.factorit.enums.Role;
import com.juanag32.factorit.repositories.ClienteRepository;
import com.juanag32.factorit.services.impl.ClienteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class ClienteServiceTest {
    @Mock
    ClienteRepository repository;

    @InjectMocks
    ClienteServiceImpl service;
    Cliente cliente;
    Cliente clienteVip;


    @BeforeEach
    void setUp() {
        cliente = new Cliente();
        cliente.setId(2L);
        cliente.setNombre("Maria");
        cliente.setRole(Role.CLIENT);
        cliente.setVip(false);
        clienteVip = new Cliente();
        clienteVip.setId(3L);
        clienteVip.setNombre("Juan");
        clienteVip.setRole(Role.CLIENT);
        clienteVip.setVip(true);
        clienteVip.setComienzoVip(LocalDateTime.now());
    }

    @Nested
    class testsServiceRead {
        @Test
        void testFindByClienteVip() {
            when(repository.findAllByVipTrue()).thenReturn(Arrays.asList(clienteVip));
            List<ClienteDto> clientes = service.clientesVip();

            assertFalse(clientes.isEmpty());
            assertTrue(clientes.get(0).isVip());
            verify(repository).findAllByVipTrue();
        }

        @Test
        void testFindByIdNotFound() {
            when(repository.findById(anyLong())).thenReturn(Optional.empty());

           NoSuchElementException exception= assertThrows(NoSuchElementException.class,() -> service.findById(3L));
           assertEquals("Cliente no encontrado",exception.getMessage());

        }

        @Test
        void testFindByIdSuccess() {
            when(repository.findById(anyLong())).thenReturn(Optional.of(clienteVip));
            ClienteDto client= service.findById(3L);

            assertNotNull(client);
            assertEquals("Juan",client.getNombre());
            assertEquals(6,client.getComienzoVip().getMonthValue());
            verify(repository).findById(anyLong());
        }
        @Test
        void testFindAll() {

            when(repository.findAll()).thenReturn(Arrays.asList(cliente,clienteVip));
            List<ClienteDto> clientes = service.findAll();
            //then
            assertFalse(clientes.isEmpty());
            assertEquals("Maria",clientes.get(0).getNombre());
            verify(repository).findAll();
        }
    }
    @Test
    void testSave() {
        //given
        ClienteRequestDto clienteRequestDto = new ClienteRequestDto();
        clienteRequestDto.setNombre("juan");
        clienteRequestDto.setApellido("aguero");
        clienteRequestDto.setEmail("juan@gmail.com");
        clienteRequestDto.setPassword("123456");
        when(repository.save(any(Cliente.class))).thenReturn(cliente);

        ClienteDto client = service.save(clienteRequestDto);

        assertNotNull(client);
        assertEquals(2,client.getId());
        verify(repository).save(any(Cliente.class));
    }
}
