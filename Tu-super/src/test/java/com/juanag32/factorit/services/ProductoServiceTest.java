package com.juanag32.factorit.services;

import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.dtos.producto.ProductoRequestDto;
import com.juanag32.factorit.entities.Producto;
import com.juanag32.factorit.enums.Categoria;
import com.juanag32.factorit.repositories.ProductoRepository;
import com.juanag32.factorit.services.impl.ProductoServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ProductoServiceTest {
    @Mock
    ProductoRepository repository;

    @InjectMocks
    ProductoServiceImpl service;
    Producto producto1;
    Producto producto2;


    @BeforeEach
    void setUp() {
        producto1 = new Producto();
        producto1.setId(1L);
        producto1.setNombre("Gaseosa Coca Cola");
        producto1.setPrecio(new BigDecimal("356"));

        producto1.setCategoria(Categoria.BEBIDAS);
        producto2 = new Producto();
        producto2.setId(2L);
        producto2.setNombre("Arroz Gallo");
        producto2.setPrecio(new BigDecimal("120"));

        producto2.setCategoria(Categoria.ALMACEN);
    }

    @Nested
    class testsRead {
        @Test
        void testFindByCategoria() {
            when(repository.findByCategoriaAndActivoTrue(any())).thenReturn(Arrays.asList(producto2));

            List<ProductoDto> productoDtos = service.findByCategoria("almacen");

            assertFalse(productoDtos.isEmpty());

            assertEquals("ALMACEN", productoDtos.get(0).getCategoria().toString());
            verify(repository).findByCategoriaAndActivoTrue(any());
        }

        @Test
        void testOrderByPrecioAsc() {
            when(repository.findAllByActivoTrueOrderByPrecioAsc()).thenReturn(Arrays.asList(producto2, producto1));

            List<ProductoDto> productoDtos = service.orderByPrecioAsc();

            assertFalse(productoDtos.isEmpty());
            assertEquals("Arroz Gallo", productoDtos.get(0).getNombre());
            assertEquals(new BigDecimal("120"), productoDtos.get(0).getPrecio());
            verify(repository).findAllByActivoTrueOrderByPrecioAsc();
        }

        @Test
        void testOrderByPrecioDesc() {
            when(repository.findAllByActivoTrueOrderByPrecioDesc()).thenReturn(Arrays.asList(producto1, producto2));

            List<ProductoDto> productoDtos = service.orderByPrecioDesc();

            assertFalse(productoDtos.isEmpty());
            assertEquals("Gaseosa Coca Cola", productoDtos.get(0).getNombre());
            assertEquals(new BigDecimal("356"), productoDtos.get(0).getPrecio());
            verify(repository).findAllByActivoTrueOrderByPrecioDesc();
        }

        @Test
        void testFindByNombre() {
            String nombre = "Arroz Gallo";
            when(repository.findByNombreIgnoreCaseContainingAndActivoTrue(anyString())).thenReturn(Arrays.asList(producto2));

            List<ProductoDto> productos = service.findByNombre(nombre);

            assertEquals("Arroz Gallo", productos.get(0).getNombre());
            assertNotNull(productos.get(0).getId());
            verify(repository).findByNombreIgnoreCaseContainingAndActivoTrue(anyString());
        }

        @Test
        void testFindByActivos() {
            when(repository.findAllByActivoTrue()).thenReturn(Arrays.asList(producto1, producto2));

            List<ProductoDto> productoDtos = service.findByActivos();

            assertFalse(productoDtos.isEmpty());
            assertEquals("Gaseosa Coca Cola", productoDtos.get(0).getNombre());
            verify(repository).findAllByActivoTrue();
        }

        @Test
        void testFindById() {
            when(repository.findById(anyLong())).thenReturn(Optional.of(producto2));

            ProductoDto producto = service.findById(2L);

            assertEquals("Arroz Gallo", producto.getNombre());
            verify(repository).findById(anyLong());
        }

        @Test
        void testFindAll() {
            when(repository.findAll()).thenReturn(Arrays.asList(producto1, producto2));

            List<ProductoDto> productoDtos = service.findAll();

            assertFalse(productoDtos.isEmpty());
            assertEquals("Gaseosa Coca Cola", productoDtos.get(0).getNombre());
            verify(repository).findAll();
        }
    }

    @Test
    void testSave() {
        ProductoRequestDto productoRequestDto = new ProductoRequestDto();
        productoRequestDto.setCategoria("bebidas");
        when(repository.save(any(Producto.class))).thenReturn(producto1);
        ProductoDto producto = service.save(productoRequestDto);
        assertEquals(1, producto.getId());

        verify(repository).save(any(Producto.class));
    }
}
