package com.juanag32.factorit.services;

import com.juanag32.factorit.entities.DiaPromocion;
import com.juanag32.factorit.repositories.DiaPromocionRepository;
import com.juanag32.factorit.services.impl.DiaPromocionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class DiaPromocionServiceTest {

    @Mock
    DiaPromocionRepository repository;

    @InjectMocks
    DiaPromocionServiceImpl service;

    DiaPromocion diaPromocion;

    @BeforeEach
    void setUp() {
        diaPromocion = new DiaPromocion(1L, LocalDate.of(2024, 6, 13), new BigDecimal("300"));
    }

    @Test
    void testFindByFechaNotFound() {

        when(repository.findByFecha(any())).thenReturn(Optional.empty());

        NoSuchElementException exception = assertThrows(NoSuchElementException.class, () -> service.findByFecha(LocalDate.now()));

        assertEquals("No hay un descuento en esta fecha", exception.getMessage());
        verify(repository).findByFecha(any());
    }

    @Test
    void testFindByFechaSuccess() {

        when(repository.findByFecha(any())).thenReturn(Optional.of(diaPromocion));
        DiaPromocion fechaPromocion = service.findByFecha(LocalDate.now());

        assertEquals(6, fechaPromocion.getFecha().getMonthValue());
        verify(repository).findByFecha(any());
    }

    @Test
    void testSaveDiaPromocion() {
        DiaPromocion diaPromoNavidad = new DiaPromocion();
        diaPromoNavidad.setFecha(LocalDate.of(2024, 12, 25));
        diaPromoNavidad.setDescuento(new BigDecimal("300"));
        when(repository.save(any(DiaPromocion.class))).thenReturn(diaPromocion);

        DiaPromocion fechaPromocion = service.save(diaPromoNavidad);


        assertNotNull(fechaPromocion.getId());
        verify(repository).save(any(DiaPromocion.class));
    }
}
