package com.juanag32.factorit.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.juanag32.factorit.dtos.cliente.ClienteDto;
import com.juanag32.factorit.dtos.cliente.ClienteRequestDto;
import com.juanag32.factorit.services.ClienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private ClienteService service;

    private ObjectMapper mapper;
    ClienteDto clienteDto;

    @BeforeEach
    void setUp() {
        mapper=new ObjectMapper();
         clienteDto = new ClienteDto(1L,"juanma","aguero","juanma@gmail.com", true,LocalDateTime.now(),null);
    }

    @Test
    void testBuscarClientesVip() throws Exception {
        when(service.clientesVip()).thenReturn(Arrays.asList(clienteDto));

        mvc.perform(get("/api/clientes/clientes-vip"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].id").value(1));
    }

    @Test
    void testSaveCliente() throws Exception {
        ClienteRequestDto clienteRequestDto = new ClienteRequestDto("juanma","aguero","juanma@gmail.com","123456");

        when(service.save(any())).thenReturn(clienteDto);

        mvc.perform(post("/api/clientes/save").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(clienteRequestDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1));
    }
}
