package com.juanag32.factorit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.juanag32.factorit.dtos.usuario.UsuarioDto;
import com.juanag32.factorit.dtos.usuario.UsuarioRequestDto;
import com.juanag32.factorit.services.UsuarioService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;


@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioService service;

    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testGuardarUsuarioSucces() throws Exception {
        UsuarioRequestDto usuarioRequestDto = new UsuarioRequestDto("Juan Manuel", "Agüero", "juanma4@gmail.com", "12313");
        UsuarioDto usuarioDto = new UsuarioDto(1L, "Juan Manuel", "Agüero", "juanma4@gmail.com", "ADMIN");
        when(service.save(any())).thenReturn(usuarioDto);

        mvc.perform(post("/api/usuarios/save").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(usuarioRequestDto)))
                .andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nombre", is("Juan Manuel")))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.rol", is("ADMIN")));

        verify(service).save(any());
    }

    @Test
    void testGuardarUsuarioBadRequest() throws Exception {
        UsuarioRequestDto usuarioRequestDto = new UsuarioRequestDto();
        usuarioRequestDto.setNombre("juan");

        mvc.perform(post("/api/usuarios/save").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(usuarioRequestDto))).
                andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
