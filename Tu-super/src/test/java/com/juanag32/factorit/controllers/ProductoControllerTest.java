package com.juanag32.factorit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.juanag32.factorit.dtos.producto.ProductoDto;
import com.juanag32.factorit.dtos.producto.ProductoRequestDto;
import com.juanag32.factorit.services.ProductoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;

@WebMvcTest(ProductoController.class)
public class ProductoControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    ProductoService service;
    private ObjectMapper mapper;
    private ProductoDto productoDto;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        productoDto = new ProductoDto(1L, "Arroz", new BigDecimal("3600"), "almacen", "GRAMOS", 300,true);
    }

    @Test
    void testGuardarProducto() throws Exception {
        ProductoRequestDto productoRequestDto = new ProductoRequestDto("Arroz", new BigDecimal("3600"), "almacen",
                "GRAMOS", 300);

        when(service.save(any())).thenReturn(productoDto);

        mvc.perform(post("/api/productos/save").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(productoRequestDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)));

    }
    @Test
    void testBuscarPorCategoria() throws Exception {

        when(service.findByNombre(anyString())).thenReturn(Arrays.asList(productoDto));

        mvc.perform(get("/api/productos/buscar-categoria/almacen"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
    @Test
    void testFindActivos() throws Exception {

        when(service.findByNombre(anyString())).thenReturn(Arrays.asList(productoDto));

        mvc.perform(get("/api/productos/activos"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
    @Test
    void testOrdenadosPorPrecioAsc() throws Exception {

        when(service.findByNombre(anyString())).thenReturn(Arrays.asList(productoDto));

        mvc.perform(get("/api/productos/precio-asc"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
    @Test
    void testOrdenadosPorPrecioDesc() throws Exception {

        when(service.findByNombre(anyString())).thenReturn(Arrays.asList(productoDto));

        mvc.perform(get("/api/productos/precio-desc"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }


    @Test
    void testBuscarPorNombre() throws Exception {

        when(service.findByNombre(anyString())).thenReturn(Arrays.asList(productoDto));

        mvc.perform(get("/api/productos/arroz"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200));
    }
}
