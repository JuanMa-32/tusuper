package com.juanag32.factorit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.juanag32.factorit.entities.DiaPromocion;
import com.juanag32.factorit.services.DiaPromocionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static javax.management.Query.value;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;


@WebMvcTest(DiaPromocionController.class)
public class DiaPromocionControllerTest {
    @MockBean
    private DiaPromocionService service;
    @Autowired
    private MockMvc mvc;
    private ObjectMapper mapper;

    DiaPromocion diaPromocion;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();
        diaPromocion = new DiaPromocion(1L, LocalDate.of(2024, 6, 14), new BigDecimal("300"));
    }

    @Test
    void testFindByFechaSuccess() throws Exception {
        LocalDate testDate = LocalDate.of(2024, 6, 14);
        when(service.findByFecha(any())).thenReturn(diaPromocion);

        mvc.perform(get("/api/dia-promocion/buscar-promocion")
                        .param("fecha", testDate.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.fecha").value(testDate.toString()));
    }


    @Test
    void testGuardarDiaPromocion() throws Exception {
       DiaPromocion diaPromocionRequest = new DiaPromocion(1L, LocalDate.of(2024, 6, 13), new BigDecimal("300"));
        when(service.save(any())).thenReturn(diaPromocion);

        mvc.perform(post("/api/dia-promocion/save").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(diaPromocionRequest)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(201))
                .andExpect((ResultMatcher) jsonPath("$.id",value(1)));
    }
}
