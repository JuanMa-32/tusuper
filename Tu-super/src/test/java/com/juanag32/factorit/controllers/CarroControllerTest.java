package com.juanag32.factorit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.juanag32.factorit.dtos.carro.CarroDto;
import com.juanag32.factorit.dtos.carro.CarroRequestDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemDto;
import com.juanag32.factorit.dtos.carroItem.CarroItemRequestDto;
import com.juanag32.factorit.services.CarroService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;

@WebMvcTest(CarroController.class)
public class CarroControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private CarroService service;
    private ObjectMapper mapper;
    private CarroDto dto;

    @BeforeEach
    void setUp() {
        mapper = new ObjectMapper();

        dto = new CarroDto(1L, LocalDate.now(), new BigDecimal("350"), new BigDecimal("350"), "VIP", null);
    }
    @Test
    void testSaveCarroBadRequest() throws Exception {
        CarroItemRequestDto carroItem = new CarroItemRequestDto();
        CarroRequestDto carroRequestdto = new CarroRequestDto(1L, new BigDecimal("350"), new BigDecimal("350"), "VIP", Arrays.asList(carroItem));

        when(service.save(any())).thenReturn(dto);

        mvc.perform(post("/api/carro/save").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(carroRequestdto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

    }
    @Test
    void testSaveCarroSuccess() throws Exception {
        CarroItemRequestDto carroItem = new CarroItemRequestDto(1L, 5, new BigDecimal("60"));
        CarroRequestDto carroRequestdto = new CarroRequestDto(1L, new BigDecimal("350"), new BigDecimal("350"), "VIP", Arrays.asList(carroItem));

        when(service.save(any())).thenReturn(dto);

        mvc.perform(post("/api/carro/save").contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(carroRequestdto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1));
    }


}
