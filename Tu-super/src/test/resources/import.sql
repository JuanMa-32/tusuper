-- Inserciones en la tabla Usuarios
INSERT INTO usuarios (id, nombre, apellido, email, password, role) VALUES (5, 'Juan', 'Perez', 'juan@example.com', 'password123', 'ADMIN');
INSERT INTO usuarios (id, nombre, apellido, email, password, role) VALUES (6, 'Maria', 'Garcia', 'maria@example.com', 'password123', 'CLIENT');
INSERT INTO usuarios (id, nombre, apellido, email, password, role) VALUES (7, 'Carlos', 'Lopez', 'carlos@example.com', 'password123', 'CLIENT');

-- Inserciones en la tabla Clientes
INSERT INTO cliente (client_id, vip, comienzo_vip, final_vip) VALUES (6, true, '2024-01-01 00:00:00', '2024-12-31 23:59:59');
INSERT INTO cliente (client_id, vip, comienzo_vip, final_vip) VALUES (7, false, NULL, NULL);

-- Insertar Productos
INSERT INTO productos (id, nombre, precio,categoria, unidad_medida, medida_valor, activo) VALUES (5, 'Manzana', 152.33,'ALMACEN', 'KILOGRAMO', 1, true);
INSERT INTO productos (id, nombre, precio,categoria, unidad_medida, medida_valor, activo) VALUES (6, 'Leche', 180.3,'BEBIDAS', 'LITRO', 1, true);
INSERT INTO productos (id, nombre, precio,categoria, unidad_medida, medida_valor, activo) VALUES (7, 'Arroz', 1533,'ALMACEN', 'KILOGRAMO', 1, true);
INSERT INTO productos (id, nombre, precio, categoria,unidad_medida, medida_valor, activo) VALUES (8, 'Agua', 1655,'BEBIDAS', 'LITRO', 1, true);

-- Insertar Dias de Promocion
INSERT INTO dias_promocion (id, fecha) VALUES (5, '2024-12-25' );
INSERT INTO dias_promocion (id, fecha) VALUES (6, '2024-06-01');
INSERT INTO dias_promocion (id, fecha) VALUES (7, '2024-11-01');
-- Insertar Carros
INSERT INTO carros (id, cliente_id, fecha_compra, subtotal,descuento,total, tipo_carro) VALUES (5, 6, '2024-06-12',180,0, 180, 'COMUN');
INSERT INTO carros (id, cliente_id, fecha_compra, subtotal,descuento, total, tipo_carro) VALUES (6, 7, '2024-06-02',180,0, 180, 'VIP');
-- Insertar CarroItems
INSERT INTO carro_items (id, producto_id, cantidad, precio_unitario, carro_id) VALUES (20, 5, 3, 1.50, 5);
INSERT INTO carro_items (id, producto_id, cantidad, precio_unitario, carro_id) VALUES (21, 6, 2, 0.80, 5);
INSERT INTO carro_items (id, producto_id, cantidad, precio_unitario, carro_id) VALUES (22, 7, 5, 2.00, 6);







