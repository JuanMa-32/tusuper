import axios from "axios";
import ClienteApi from './../auth/apis/ClienteApi';

const URL = 'http://localhost:8080/api/clientes'

export const getClientesVip = async() => {
    try {
        const response = await ClienteApi.get(`${URL}/clientes-vip`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const getClienteFindById = async(id) => {
    try {
        const response = await ClienteApi.get(`${URL}/${id}`)
        return response;
    } catch (error) {
        throw error;
    }
}


export const registroCliente = async(cliente) => {
    try {
        const response = await ClienteApi.post(`${URL}/save`,cliente)
        return response;
    } catch (error) {
        throw error;
    }
}
export const cancelarVipCliente = async(idCLiente) => {
    try {
        const response = await ClienteApi.put(`${URL}/cancelar-vip/${idCLiente}`)
        return response;
    } catch (error) {
        throw error;
    }
}