import axios from "axios";
import DiaPromocionApi from './../auth/apis/DiaPromocionApi';

const URL = 'http://localhost:8080/api/dia-promocion'

export const getFechaPromocion = async(fecha) => {
    try {
        const response = await DiaPromocionApi.get(`${URL}/buscar-promocion?fecha=${fecha}`)
        return response;
    } catch (error) {
        throw error;
    }
}

export const saveDiaPromocion = async(fecha) => {
    try {
        const response = await DiaPromocionApi.post(`${URL}/save`,fecha)
        return response;
    } catch (error) {
        throw error;
    }
}