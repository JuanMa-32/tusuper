
import ProductoApi from './../auth/apis/ProductoApi';

const URL = ''
export const saveProducto = async(producto) => {
    try {
        const response = await ProductoApi.post(`${URL}/save`,producto)
        return response;
    } catch (error) {
        throw error;
    }
}
export const cambiarEstadoProducto = async(id) => {
    try {
        const response = await ProductoApi.put(`${URL}/cambiar-estado/${id}`,)
        return response;
    } catch (error) {
        throw error;
    }
}

export const findByNombre = async (nombre) => {
    try {
        const response = await ProductoApi.get(`${URL}/${nombre}`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const orderByPrecioAsc = async () => {
    try {
        const response = await ProductoApi.get(`${URL}/precio-asc`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const orderByPrecioDesc = async () => {
    try {
        const response = await ProductoApi.get(`${URL}/precio-desc`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const findByCategoria = async (categoria) => {
    try {
        const response = await ProductoApi.get(`${URL}/buscar-categoria/${categoria}`)
        return response;
    } catch (error) {
        throw error;
    }
}

export const findAllActivos= async () => {
    try {
        const response = await ProductoApi.get(`${URL}/activos`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const findAll= async () => {
    try {
        const response = await ProductoApi.get(`${URL}/all`)
        return response;
    } catch (error) {
        throw error;
    }
}


