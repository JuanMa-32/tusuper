import axios from "axios";
import CarroApi from './../auth/apis/CarroApi';

const URL = 'http://localhost:8080/api/carro'

export const saveCarro = async(carro) => {
    try {
        const response = await CarroApi.post(`${URL}/save`,carro)
        return response;
    } catch (error) {
        throw error;
    }
}

export const carrosClient = async(idCliente) => {
    try {
        const response = await CarroApi.get(`${URL}/carros/client/${idCliente}`)
        return response;
    } catch (error) {
        throw error;
    }
}
export const carroFindById = async(idCarro) => {
    try {
        const response = await CarroApi.get(`${URL}/${idCarro}`)
        return response;
    } catch (error) {
        throw error;
    }
}