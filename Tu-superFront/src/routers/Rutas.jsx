import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { Inicio } from '../page/Inicio'
import { NavBar } from '../components/NavBar'
import { CarroCompra } from '../components/Carro/CarroCompra'
import { SegundoNavBar } from '../components/SegundoNavBar'
import { MisCompras } from '../components/cliente/MisCompras'
import { AdminProductos } from './../page/AdminProductos';
import { AdminClientesVip } from '../page/AdminClientesVip'
import { MisComprasPage } from '../page/MisComprasPage'
import { CarroConfirmacion } from '../components/Carro/CarroConfirmacion'
import { AppProvider } from '../context/AppProvider'
import { ProductosCategoriaPage } from '../page/ProductosCategoriaPage'
import { Login } from '../auth/pages/Login'
import { FormProducto } from '../components/producto/FormProducto'
import { AuthProvider } from '../auth/context/AuthProvider'
import { CarroConfirmacionView } from '../components/Carro/CarroConfirmacionView'

export const Rutas = () => {
    return (
        <AuthProvider>
            <AppProvider>
                <div className="container justify-content-center ">
                    <NavBar />
                    <SegundoNavBar />
                    <Routes>
                        <Route path="" element={<Inicio />} />
                        <Route path="/" element={<Navigate to="" />} />
                        <Route path="/mis/compras/:idCliente" element={<MisComprasPage />} />
                        <Route path='/compra/confirmar' element={<CarroConfirmacion />} />
                        <Route path='/carro/view/:idCarro' element={<CarroConfirmacionView />} />
                        <Route path="/productos/all" element={<AdminProductos />} />
                        <Route path="/productos/:categoria" element={<ProductosCategoriaPage />} />
                        <Route path="/producto/form" element={<FormProducto />} />
                        <Route path="/clientes/vip" element={<AdminClientesVip />} />
                        <Route path="/ingresar" element={<Login />} />
                    </Routes>
                </div>
            </AppProvider>
        </AuthProvider>
    )
}
