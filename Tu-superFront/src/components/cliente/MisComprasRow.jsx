import { faMoneyCheckDollar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import { Link } from 'react-router-dom';

export const MisComprasRow = ({carro}) => {
  
    return (
        <>
            <tr> 
                <td >{carro.fechaCompra} </td>
                <td >{carro.tipoCarro} </td>
                <td > {carro.items.length}  </td>
                <td ><FontAwesomeIcon icon={faMoneyCheckDollar} size="sm" /> $ {carro.total} </td>
                <td ><Link className="btn" style={{ background: '#74C0FC', color: 'white' }} to={`/carro/view/${carro.id}`}>Ver Compra </Link> </td>
            </tr>
        </>
    )
}
