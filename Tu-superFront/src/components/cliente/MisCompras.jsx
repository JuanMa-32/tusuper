import React, { useContext } from 'react'
import { MisComprasRow } from './MisComprasRow'
import { AppContext } from '../../context/AppContext'

export const MisCompras = () => {
  const { carroCliente } = useContext(AppContext);

  return (

    <table className="table table-hover table-light mt-3 caption-top" >
      <thead >
        <tr >
          <th style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Fecha</th>
          <th style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Carro</th>
          <th style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Cant. items</th>
          <th style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Total</th>
          <th style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}></th>
        </tr>
      </thead>
      <tbody>
        {carroCliente.map(carro => (
          <MisComprasRow carro={carro} key={carro.id} />
        ))}

      </tbody>
    </table>
  )
}
