import React, { useContext } from 'react'
import { Link } from 'react-router-dom';
import { UseDiaPromocion } from './../hooks/UseDiaPromocion';
import { AppContext } from './../context/AppContext';
import { FechaPromocionModal } from './diaPromocion/FechaPromocionModal';

export const SegundoNavBar = () => {
  const{ handlerOpenForm,visibleFrom} = useContext(AppContext)

  const admin = sessionStorage.getItem('isAdmin');

  return (
    <nav className="navbar navbar-expand-lg " style={{ background: '#74C0FC', color: 'white' }}>
      {!visibleFrom || <FechaPromocionModal/>  }
      <div className="container-fluid justify-content-center">
        <div className="navbar-nav">
          {admin === 'true' ?
            (<>
              <Link to={'/clientes/vip'} className="nav-link btn mx-1">Clientes Vip</Link>
              <Link to={'/productos/all'} className="nav-link btn mx-1">Productos</Link>
              <button onClick={handlerOpenForm} to={'/productos/all'} className="nav-link btn mx-1">Lanzar Promoción</button>

            </>) :
            (<>
              <Link  to={'/productos/Bebidas'} className="nav-link btn mx-1">Bebidas</Link>
              <Link  to={'/productos/Almacen'} className="nav-link btn mx-1">Almacen</Link>
              <Link  to={'/productos/Electro'} className="nav-link btn mx-1">Electro</Link>
              <Link  to={'/productos/Limpieza'} className="nav-link btn mx-1">Limpieza</Link>
            </>)}

        </div>
      </div>
    </nav>
  );
}
