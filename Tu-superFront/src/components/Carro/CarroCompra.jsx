import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { AppContext } from '../../context/AppContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { getFechaPromocion } from '../../services/diaPromocionService';

export const CarroCompra = () => {
    const { carroItem, setCarroEstado, diaPromocion } = useContext(AppContext);
    const [descuento, setDescuento] = useState(0);
    const [subtotal, setsubtotal] = useState(0);
    const [tipoCarro, setTipoCarro] = useState('comun');
    const [total, setTotal] = useState(0);

    const id = sessionStorage.getItem('id');
    const isVip = sessionStorage.getItem('isVip');

    const verificarTipoCarro = async () => {
        try {
            if (isVip =='true') {
                setTipoCarro('vip');
            } else {
                const fecha = new Date();
                const year = fecha.getFullYear();
                const month = String(fecha.getMonth() + 1).padStart(2, '0');
                const day = String(fecha.getDate()).padStart(2, '0');

                const esPromocionable = await getFechaPromocion(`${year}/${month}/${day}`)
                if (esPromocionable.status === 200) {
                    setTipoCarro('promocionable');
                }
            }
        } catch (error) {
            console.error('Error verificando el tipo de carro:', error);
        }
    };
    const calcularDescuento = () => {
        const numProductos = carroItem.length;
        let descuento = 0;
       
        if (numProductos === 4) {
            descuento = total * 0.25;
        } else if (numProductos > 5) {
            if (tipoCarro === 'comun') {
                descuento = 100;
            } else if (tipoCarro === 'promocionable') {
                descuento = 300;
            } else if (tipoCarro === 'vip') {
                const productoMasBarato = Math.min(...carroItem.map(item => item.product.precio));
                descuento = productoMasBarato + 500;
            }
        }
        setDescuento(descuento);
    };

    const actualizarTotal = () => {
        const nuevoTotal = carroItem.reduce((acc, item) => acc + (item.quantity * item.product.precio), 0);
        setsubtotal(nuevoTotal)
        setTotal(nuevoTotal - descuento);
    };

    useEffect(() => {
        verificarTipoCarro();
    }, []);

    useEffect(() => {
        calcularDescuento();
        actualizarTotal();
    }, [carroItem, tipoCarro]);



    const pagar = (event) => {
        event.preventDefault()
        setCarroEstado(carroItem, total, subtotal,descuento, id, tipoCarro)
        navigate('/compra/confirmar')
    }
    const navigate = useNavigate();
    return (
        <div style={{ border: 'solid, 1px', borderColor: '#e6e2e3' }}>
            {carroItem.length > 0 ?
                (<>
                <div>
            {tipoCarro === 'vip' && <p>Este es un carro <b>VIP</b>VIP!si llevas mas de 5 Productos Diferentes tendras un descuento de $500 y bonificaremos el producto mas barato </p>}
            {tipoCarro === 'promocionable' && <p>Este es un carro  <b>Promocionable</b>, si llevas mas de 5 Productos Diferentes tendras un descuento de $300</p>}
            {tipoCarro === 'comun' && <p>Este es un carro  <b>Común</b>, si compras 4 productos DIFERENTES tendras un descuento del 25%</p>}
          </div>
                    <table className="table mt-3">
                        <tbody >
                            {carroItem.map(item => (
                                <tr key={item.id}>
                                    <td>{item.quantity} </td>
                                    <td className='col-6'>
                                        {item.product.nombre}
                                    </td>
                                    <td>
                                        <div >$  {item.product.precio}</div>
                                    </td>

                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <div className="text-end" style={{ marginTop: '420px' }}>
                        <hr />
                        <p>Subtotal: ${subtotal}</p>
                        <p>Descuento: ${descuento} </p>

                        <h6>Total: ${total}</h6>
                        <button
                            className='btn col-12 mb-2'
                            style={{ background: '#74C0FC', color: 'white' }}

                            onClick={pagar}
                        >
                            Ir al pago
                        </button>
                    </div>
                </>) :
                (
                    <div>
                        <h4>Tu carrito está vacío<FontAwesomeIcon icon={faCartShopping} size="xl" style={{ color: "#74C0FC", }} /></h4>
                        <p>Cliclea en los artículos para añadirlos a la compra.</p>
                    </div>
                )}

        </div>
    )
}
