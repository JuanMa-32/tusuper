import React, { useEffect, useState } from 'react'

import { faDollarSign, faEllipsisH, faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { carroFindById } from './../../services/CarroService';
import { Link, useParams } from 'react-router-dom';

export const CarroConfirmacionView = () => {

    const [mostrarInfoAdicional, setMostrarInfoAdicional] = useState(false);
    const [carro, setCarro] = useState({});

    const { idCarro } = useParams()

    const getCarro = async () => {
        const response = await carroFindById(idCarro);
        setCarro(response.data)
    }

    useEffect(() => {  
        getCarro()
    }, [idCarro])


    const handleToggleInfo = () => {
        setMostrarInfoAdicional(!mostrarInfoAdicional);
    };



    return (
        <div className="row mx-auto">
            <div className="col-md-6 mt-5">
                <div className={`mb-3 p-4 ${mostrarInfoAdicional ? 'expandir' : ''}`} style={{ background: '#f7f7f8', width: '100%', padding: '10px', borderRadius: '10px', position: 'relative' }}>
                    <h5 className="mb-0">Items</h5>
                    <button type="button" className="btn btn-light" onClick={handleToggleInfo} style={{ background: '#bcbabf',position: 'absolute', top: '10px', right: '10px' }}>
                        <FontAwesomeIcon icon={faEllipsisH} />
                    </button>
                    {mostrarInfoAdicional && (
                        <table className="table table-borderless mt-2 table-light">
                            <tbody >
                                {carro.items.map(item => (
                                    <tr key={item.id}>
                                        <td>{item.cantidad} </td>
                                        <td className='col-6'>{item.producto.nombre}</td>
                                        <td>$ {item.producto.precio}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    )}
                </div>
            </div>

            <div className="col-md-6 mt-5">
                <div className="mb-3 p-4" style={{ background: '#faf6f6', width: '100%', padding: '10px', borderRadius: '10px', display: 'flex', justifyContent: 'space-between' }}>
                    <table className='table'>
                        <tr>
                            <th>Subtotal: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.subtotal} </th>
                        
                        </tr>
                        <tr>
                        <th>Descuento: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.descuento} </th>
                        </tr>

                        <tr className=''>
                            <th>Total: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.total} </th>
                        </tr>
                    </table>
                </div>


                <Link to={'/'} className="btn mt-3 btn-light" >Volver</Link>
            </div>
        </div>

    )
}

