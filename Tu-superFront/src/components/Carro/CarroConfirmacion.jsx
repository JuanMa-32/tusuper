import { faDollarSign, faEllipsisH, faMoneyBill } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { AppContext } from '../../context/AppContext';

export const CarroConfirmacion = () => {
    const { carro, carroItem, guardarCompraConfirmada, restablecerCarro } = useContext(AppContext)
    const [mostrarInfoAdicional, setMostrarInfoAdicional] = useState(false);

    const handleToggleInfo = () => {
        setMostrarInfoAdicional(!mostrarInfoAdicional);
    };


    const navigate = useNavigate();
    const cancelarCompra = () => {
        restablecerCarro()
        navigate('/')
    }

    const onSubmit = (event) => {
        event.preventDefault()
        guardarCompraConfirmada()
        restablecerCarro()
    }

    return (
        <div className="row mx-auto">
            <div className="col-md-6 mt-5">
                <div className={`mb-3 p-4 ${mostrarInfoAdicional ? 'expandir' : ''}`} style={{ background: '#faf6f6', width: '100%', padding: '10px', borderRadius: '10px', position: 'relative' }}>
                    <h5 className="mb-0">Items</h5>
                    <button type="button" className="btn btn-light" onClick={handleToggleInfo} style={{ position: 'absolute', top: '10px', right: '10px' }}>
                        <FontAwesomeIcon icon={faEllipsisH} />
                    </button>
                    {mostrarInfoAdicional && (
                        <table className="table table-borderless mt-2 table-light">
                            <tbody >
                                {carroItem.map(item => (
                                    <tr key={item.id}>
                                        <td>{item.quantity} </td>
                                        <td className='col-6'>{item.product.nombre}</td>
                                        <td>$ {item.product.precio}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    )}
                </div>
            </div>

            <div className="col-md-6 mt-5">
                <div className="mb-3 p-4" style={{ background: '#faf6f6', width: '100%', padding: '10px', borderRadius: '10px', display: 'flex', justifyContent: 'space-between' }}>
                    <table className='table'>
                        <tr>
                            <th>Subtotal: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.subtotal} </th>
                        </tr>
                        <tr>
                        <th>Descuento: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.descuento} </th>
                        </tr>

                        <tr className=''>
                            <th>Total: </th>
                            <th></th>
                            <th></th>
                            <th>{carro.total} </th>
                        </tr>
                    </table>
                </div>

                <button className='btn mt-3 mx-2'
                    onClick={onSubmit}
                    style={{ background: "#74C0FC", color: 'white' }}
                ><FontAwesomeIcon icon={faDollarSign} /> Finalizar compra</button>
                <button onClick={cancelarCompra} className="btn mt-3 btn-light" >Descartar compra</button>
            </div>
        </div>

    )
}
