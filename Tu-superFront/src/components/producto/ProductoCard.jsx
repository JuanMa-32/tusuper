import { faMinus, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useState } from 'react'
import { AppContext } from '../../context/AppContext';
import { Link } from 'react-router-dom';

export const ProductoCard = ({ producto }) => {
  const [cantidad, setCantidad] = useState(0);
const{ handlerAddProductCart, restarProducto, handlerDeleteProductCart}=useContext(AppContext)
  const handleIncrement = () => {
    setCantidad(cantidad + 1);
    handlerAddProductCart(producto)
  };

  const handleDecrement = () => {
    if (cantidad > 0) {
      setCantidad(cantidad - 1);
      restarProducto(producto)
    }
  };
  const handleTrashClick = () => {
    setCantidad(0);
    handlerDeleteProductCart(producto.id)
  };

  const isAuth=sessionStorage.getItem('isAuth');

  return (
    <div className="card" style={{ width: '16rem', marginTop: '10px', marginLeft: '18px' }}>
      <img src="/logoTusuper.jpg" className="card-img-top" alt="..." />
      <div className="card-body">
        <h6 className="card-title">{producto.nombre} {producto.medidaValor} {producto.unidadMedida}</h6>
        <h5 className="card-title">
          $ {producto.precio}
        </h5>
        <div className="d-flex justify-content-between align-items-center">
          {isAuth=='true' ? 
          (<>
            <button className="btn btn-sm" style={{ background: "#74C0FC", color: 'white' }} onClick={handleIncrement}>
            <FontAwesomeIcon icon={faPlus} />
          </button>
          {cantidad > 0 && (
            <>
              {cantidad > 1 ? (
                <button className="btn btn-sm btn-danger" onClick={handleDecrement}>
                  <FontAwesomeIcon icon={faMinus} />
                </button>
              ) : (
                <button className="btn btn-sm btn-danger" onClick={handleTrashClick}>
                  <FontAwesomeIcon icon={faTrash} />
                </button>
              )}
              <span>{cantidad}</span>
            </>
          )}
          </>):
          (<>
            <Link to={'/ingresar'} className="btn btn-sm"
             style={{ background: "#74C0FC", color: 'white' }}>
            <FontAwesomeIcon icon={faPlus} />
          </Link>
          </>)}
        
        </div>
      </div>
    </div>
  );
}
