import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AppContext } from '../../context/AppContext';

export const FormProducto = () => {
    const [productoForm, setproductoForm] = useState({})
    const { guardarProducto, errorProducto } = useContext(AppContext)
    const onInputChange = ({ target }) => {
        const { name, value } = target;
        setproductoForm({
            ...productoForm,
            [name]: value
        })
    }

    const onSubmit = (event) => {
        event.preventDefault()
        console.log(productoForm);
        guardarProducto(productoForm)
    }

    return (
        <>
            <form onSubmit={onSubmit}>
                <div className="container mt-5">
                    <h6>Agrega un producto</h6>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="mb-4 p-4" style={{ background: 'white', borderRadius: '10px', boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)' }}>
                                <p className='text-danger'>  {errorProducto.nombre}</p>
                                <div className="col-9 mb-4 d-flex mx-auto">
                                    <input type="text" style={{ borderRadius: '8px' }}
                                        className="form-control  mx-auto" id="exampleFormControlInput1"
                                        placeholder="Nombre"
                                        name="nombre"
                                        onChange={onInputChange}
                                        value={productoForm?.nombre}
                                    />
                                </div>
                                <p className='text-danger'>  {errorProducto.precio}</p>
                                <div className="col-9 mb-4 d-flex mx-auto">
                                    <input type="number" className="form-control" style={{ borderRadius: '8px' }}
                                        placeholder="Precio"
                                        aria-label="Amount (to the nearest dollar)"
                                        name="precio"
                                        onChange={onInputChange}
                                        value={productoForm?.precio}
                                    />
                                </div>
                                <p className='text-danger'>  {errorProducto.categoria}</p>
                                <div className="col-9 mb-4 d-flex mx-auto">
                                    <label className="input-group-text" for="inputGroupSelect01">Categoria</label>
                                    <select
                                        className="form-select"
                                        id="inputGroupSelect01"
                                        onChange={onInputChange}
                                        value={productoForm?.categoria}
                                        name="categoria"
                                    >
                                        <option value="" disabled selected>Seleccionar </option>
                                        <option value="Bebidas" selected={productoForm?.categoria === "Bebidas"}>
                                            Bebidas
                                        </option>
                                        <option value="Limpieza" selected={productoForm?.categoria === "Limpieza"}>
                                            Limpieza
                                        </option>
                                        <option value="Electro" selected={productoForm?.categoria === "Electro"}>
                                            Electro
                                        </option>
                                        <option value="Almacen" selected={productoForm?.categoria === "Almacen"}>
                                            Almacen
                                        </option>

                                    </select>

                                </div>
                                <p className='text-danger'>  {errorProducto.medidaValor}</p>
                                <div className="col-9 mb-4 d-flex mx-auto">
                                    <input type="number" className="form-control" style={{ borderRadius: '8px' }}
                                        placeholder="Medida valor "
                                        aria-label="Amount (to the nearest dollar)"
                                        name="medidaValor"
                                        onChange={onInputChange}
                                        value={productoForm?.medidaValor}
                                    />
                                </div>
                                <p className='text-danger'>  {errorProducto.unidadMedida}</p>
                                <div className="col-9 mb-4 d-flex mx-auto">
                                    <label className="input-group-text" for="inputGroupSelect01">Unidad de Medida</label>
                                    <select
                                        className="form-select"
                                        id="inputGroupSelect01"
                                        onChange={onInputChange}
                                        value={productoForm?.venederPor}
                                        name="unidadMedida"
                                    >
                                        <option value="" disabled selected>Seleccionar </option>
                                        <option value="KILOGRAMO" selected={productoForm?.venederPor === "KILOGRAMO"}>
                                            KILOGRAMO
                                        </option>
                                        <option value="GRAMO" selected={productoForm?.venederPor === "GRAMO"}>
                                            GRAMO
                                        </option>
                                        <option value="LITRO" selected={productoForm?.venederPor === "LITRO"}>
                                            LITRO
                                        </option>
                                        <option value="MILILITRO" selected={productoForm?.venederPor === "MILILITRO"}>
                                            MILILITRO
                                        </option>
                                        <option value="UNIDAD" selected={productoForm?.venederPor === "UNIDAD"}>
                                            UNIDAD
                                        </option>
                                        <option value="PAQUETE" selected={productoForm?.venederPor === "PAQUETE"}>
                                            PAQUETE
                                        </option>
                                        <option value="CAJA" selected={productoForm?.venederPor === "CAJA"}>
                                            CAJA
                                        </option>

                                    </select>

                                </div>

                                <button className="btn mt-3" style={{ background: '#74C0FC', color: 'white' }}
                                    type="submit">Enviar</button>
                                <Link to={'/productos'} className="btn mt-3" >Cancelar</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </>
    )
}
