import React, { useContext } from 'react'
import { ProductoCard } from './ProductoCard';
import { AppContext } from './../../context/AppContext';

export const Productos = () => {
  const { productos } = useContext(AppContext);
  
  return (
    <div className="container">
      <div className="row">
        {productos?.map(producto =>
          <ProductoCard producto={producto} key={producto.id} />
        )}
       
      </div>
    </div>
  );
}
