import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom';
import { AppContext } from '../context/AppContext';
import { AuthContext } from './../auth/context/AuthContext';

export const NavBar = () => {
  const { buscarPorNombre } = useContext(AppContext);
  const {handlerLogout} = useContext(AuthContext)
  const [producto, setproducto] = useState('')

  const onInputChange = ({ target }) => {
    setproducto(target.value)
  }
  const onSubmit = (event) => {
    event.preventDefault();
    if (producto == '') {
      return
    }
    buscarPorNombre(producto);
  }
const login=sessionStorage.getItem('isAuth');
const role=sessionStorage.getItem('rol');
const id=sessionStorage.getItem('id');

  return (
      <nav className="navbar navbar-expand-lg">
        <div className="container">
          <Link className="navbar-brand" to={'/'}>
            <img src='/logoTusuper.jpg' alt="Logo de Supermercado" style={{ maxWidth: '65px' }} />
          </Link>
  {role==='ADMIN' ? (<></>):
  <>
    <form onSubmit={onSubmit} className="d-flex mx-auto">
            <input
              className="form-control me-6"
              type="search"
              placeholder="¿Qué querés comprar?"
              aria-label="Buscar"
              onChange={onInputChange}
              name='producto'
              value={producto}
              style={{ maxWidth: '300px' }}
            />
            <button className="btn" style={{ background: '#74C0FC' }} type="submit">
              <FontAwesomeIcon icon={faMagnifyingGlass} style={{ color: 'white' }} />
            </button>
          </form>
  
  </>}
        
          <div className="ms-auto">
            {!login ? (
              <Link to="/ingresar" className="btn" style={{ background: '#74C0FC', color: 'white' }}>
                Ingresar
              </Link>
            ) : (
              <>
                {role === 'ADMIN' ? (
                  <button className="btn " style={{ background: '#e6e2e3'}} onClick={handlerLogout}>
                    Cerrar sesión
                  </button>
                ) : (
                  <>
                    <Link to={`/mis/compras/${id}`}  className="btn  me-2" style={{ background: '#74C0FC', color: 'white' }}>
                      Mis compras
                    </Link>
                    <button className="btn "style={{ background: ' #e6e2e3' }} onClick={handlerLogout}>
                      Cerrar sesión
                    </button>
                  </>
                )}
              </>
            )}
          </div>
        </div>
      </nav>
    );
};
