import React, { useContext } from 'react'
import { AppContext } from './../../context/AppContext';

export const ClienteRow = ({ cliente }) => {
    const{darDeBajaVip}=useContext(AppContext);
    return (
        <tr>
            <td>{cliente.nombre} </td>
            <td>{cliente.email} </td>
            <td>{cliente.comienzoVip} </td>
            <td>{cliente.finalVip} </td>
            {cliente.finalVip ?
                (<>
                <td></td>
                </>) :
                (<>
                    <td>
                        <button className='btn btn-sm ms-2'
                            style={{ background: '#fd365b', color: 'white' }}
                            onClick={()=> darDeBajaVip(cliente.id)}
                            type='submit' >
                            Cancelar Vip
                        </button>
                    </td>
                </>)}


        </tr>
    )
}
