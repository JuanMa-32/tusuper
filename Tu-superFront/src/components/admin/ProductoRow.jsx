import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useContext } from 'react'
import { AppContext } from './../../context/AppContext';

export const ProductoRow = ({ producto }) => {
    const { handlerEstadoProducto } = useContext(AppContext)
    return (
        <>
            <tr >
                <td >{producto.nombre}</td>
                <td>{producto.precio}</td>
                <td>{producto.categoria}</td>
                {producto.activo ? 
                (<>
                 <td>
                    <button className='btn btn-sm'  style={{ background: '#fd365b', color: 'white' }}
                     onClick={() => handlerEstadoProducto(producto.id)}> Dar de baja</button></td>
                </>)
                :
                (<>
                  <td><button className='btn btn-sm'  style={{ background: '#74C0FC', color: 'white' }}
                  onClick={() => handlerEstadoProducto(producto.id)}> Dar de alta</button></td>
                </>)}
             
            </tr>
        </>
    )
}
