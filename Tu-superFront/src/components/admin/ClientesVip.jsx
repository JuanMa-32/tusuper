import React, { useContext } from 'react'
import { ClienteRow } from './ClienteRow'
import { AppContext } from './../../context/AppContext';

export const ClientesVip = () => {
  const { clientesVip } = useContext(AppContext)
  return (
    <table className="table table-hover table-light m-2 caption-top">
      <thead>
        <tr>
          <th scope="col" style={{ color: '#92939e', fontFamily: 'Kanit, sans-serif' }}>Nombre</th>
          <th scope="col" style={{ color: '#92939e', fontFamily: 'Kanit, sans-serif' }}>Email</th>
          <th scope="col" style={{ color: '#92939e', fontFamily: 'Kanit, sans-serif' }}>Inicio Vip</th>
          <th scope="col" style={{ color: '#92939e', fontFamily: 'Kanit, sans-serif' }}>Finalización Vip</th>
          <th scope="col" style={{ color: '#92939e', fontFamily: 'Kanit, sans-serif' }}>Dar Baja</th>
        </tr>
      </thead>
      <tbody>
        {clientesVip.map(cliente => (

          <ClienteRow cliente={cliente} key={cliente.id} />
        ))}

      </tbody>
    </table>
  )
}
