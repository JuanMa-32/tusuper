import React, { useContext } from 'react'
import { ProductoRow } from './ProductoRow'
import { AppContext } from '../../context/AppContext';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

export const ProductosList = () => {
    const { productos, productosOrdenadosXPrecioAsc, productosOrdenadosXPrecioDesc } = useContext(AppContext);

    const orderAsc = () => {
        productosOrdenadosXPrecioAsc()
    }
    const orderDesc = () => {
        productosOrdenadosXPrecioDesc()
    }

    return (
        <>
            <div className="d-flex align-items-center justify-content-between mb-3" style={{ width: '100%', padding: '10px', borderRadius: '10px' }}>
                <div className="d-flex align-items-center">
                    <button onClick={orderAsc} className='btn me-2' style={{ color: '#74C0FC' }}>Ordenar por mas baratos</button>
                    <button onClick={orderDesc} className='btn me-2' style={{ color: '#74C0FC' }}>Ordenar por mas caros</button>

                </div>
                <Link to={'/producto/form'} className="btn m-1" style={{ background: '#74C0FC', color: 'white' }}>
                    <FontAwesomeIcon icon={faPlus} /> Producto
                </Link>
            </div>
            <table className="table table-hover table-light caption-top" >
                <thead >
                    <tr >
                        <th className="col-3" style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Nombre</th>
                        <th className="col-1" style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Precio</th>
                        <th className="col-1" style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}>Categoria</th>
                        <th className="col-1" style={{ color: '#92939e', fontfamily: 'Kanit, sans-serif' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {productos?.map(producto =>
                        <ProductoRow producto={producto} key={producto.id} />
                    )}
                </tbody>
            </table>
        </>
    )
}
