import { FechaPromocionForm } from "./FechaPromocionForm";
import React from 'react'

export const FechaPromocionModal = () => {
    return (
        <div className='abrir-modal animacion fadeIn'>
            <div className='modal' style={{ display: "block" }} tabIndex="-1">
                <div className='modal-dialog ' role='document'>
                    <div className='modal-content'>
                        <div className='modal-header'>
                            <h5 className='modal-title' style={{color:'black'}}>
                               Formulario Oferta
                            </h5>
                        </div>
                        <div className='modal-body'>
                            <FechaPromocionForm />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

