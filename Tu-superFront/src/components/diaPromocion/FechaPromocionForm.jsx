import React, { useContext, useState } from 'react'
import { AppContext } from '../../context/AppContext'

export const FechaPromocionForm = () => {
  const { handlerCloseForm, errors, guardarPromocion } = useContext(AppContext)

  const [promocion, setpromocion] = useState({});

  const onInputChange = ({ target }) => {
    const { name, value } = target;
    setpromocion({
      ...promocion,
      [name]: value
    })
  }
  const onSubmit = (event) => {
    event.preventDefault();
    
    guardarPromocion(promocion)

  }

  return (

    <form onSubmit={onSubmit}>
      <p className="text-danger">{errors?.fecha} </p>
      <div className="mb-3">
        <input
          type="date"
          className="form-control mt-3"
          style={{ borderRadius: '8px' }}
          name="fecha"
          onChange={onInputChange}
          value={promocion.fecha}
        />
        <p className="text-danger">{errors?.descuento} </p>
      </div>

      <div className="d-flex justify-content-start mt-3">
        <button className='btn btn-light btn-sm' type='button' onClick={handlerCloseForm}>
          Cancelar
        </button>
        <button className='btn btn-sm ms-2'
          style={{ background: '#74C0FC', color: 'white' }}
          type='submit' >
          Lanzar Oferta
        </button>
      </div>
    </form>

  )
}
