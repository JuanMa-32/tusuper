import React, { useContext, useEffect } from 'react'
import { MisCompras } from '../components/cliente/MisCompras'
import { useParams } from 'react-router-dom';
import { AppContext } from '../context/AppContext';

export const MisComprasPage = () => {
  const{idCliente} = useParams()
  const{getcarrosCliente} = useContext(AppContext);
  useEffect(() => {
    getcarrosCliente(idCliente)
  }, [idCliente])
  const email = sessionStorage.getItem('email');
  return (
    <>
      <h5 className='mt-2 mb-5'>Hola {email}, estas son tus compras.</h5>
      <MisCompras />
    </>
  )
}
