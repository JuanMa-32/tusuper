import React, { useContext, useEffect } from 'react'
import { ClientesVip } from '../components/admin/ClientesVip'
import { AppContext } from './../context/AppContext';

export const AdminClientesVip = () => {

  const {buscarClientesVip} = useContext(AppContext)

  useEffect(() => {
    buscarClientesVip()
  }, [])
  
  return (
    <>
    <h6 className='mt-3'>Clientes Vip.</h6>
    <ClientesVip/>
    </>
  )
}
