import React, { useContext, useEffect } from 'react'
import { Productos } from '../components/producto/Productos'
import { CarroCompra } from '../components/Carro/CarroCompra'
import { AppContext } from '../context/AppContext'
import { useParams } from 'react-router-dom'

export const Inicio = () => {
 
  const {productosActivos} = useContext(AppContext);
  useEffect(() => {
    productosActivos()
    
  }, [])
  
  return (
    <div className="">
      <div className="row">
        <div className="col-md-8">
          <Productos />
        </div>
        <div className="col-md-3">
          <CarroCompra />
        </div>
      </div>
    </div>
  );
}
