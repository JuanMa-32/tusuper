import React, { useContext, useEffect } from 'react'
import { ProductosList } from '../components/admin/ProductosList'
import { AppContext } from '../context/AppContext';

export const AdminProductos = () => {
  const {productosAll} = useContext(AppContext);
  useEffect(() => {
    productosAll()
  }, [])
  return (
    <>
<ProductosList/>
    </>
  )
}
