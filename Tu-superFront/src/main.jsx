import React from 'react'
import ReactDOM from 'react-dom/client'

import './index.css'
import TuSupermercadoApp from './TuSupermercadoApp'
import { BrowserRouter } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <TuSupermercadoApp />
    </BrowserRouter>
  </React.StrictMode>,
)
