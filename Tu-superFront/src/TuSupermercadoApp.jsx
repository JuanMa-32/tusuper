import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import { Rutas } from './routers/Rutas'
import { Routes } from 'react-router-dom'

function TuSupermercadoApp() {


  return (
    <>
      <Rutas />
    </>
  )
}

export default TuSupermercadoApp
