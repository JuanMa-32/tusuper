import { useState } from "react"
import { getFechaPromocion, saveDiaPromocion } from './../services/diaPromocionService';
import Swal from "sweetalert2";


export const UseDiaPromocion = () => {
  const [diaPromocion, setdiaPromocion] = useState({})
  const [visibleFrom, setvisibleFrom] = useState(false);
  const [errors, seterrors] = useState()

  const handlerOpenForm = () => {
    setvisibleFrom(true);
  }
  const handlerCloseForm = () => {
    setvisibleFrom(false)
    seterrors({})
  }

  const guardarPromocion = async (diaPromocion) => {
   
    try {
      const response = await saveDiaPromocion(diaPromocion)
      Swal.fire
        ('Exito', 'La promocion fue lanzada con exito', 'success')
        handlerCloseForm()
    } catch (error) {
      if (error.response && error.response.status == 400) {
        seterrors(error.response.data)
      }
    }

  }

  const buscarFechaPromocion = async () => {
    const fecha = new Date();
    const year = fecha.getFullYear();
    const month = String(fecha.getMonth() + 1).padStart(2, '0');
    const day = String(fecha.getDate()).padStart(2, '0');
    try {
      const response = await getFechaPromocion(`${year}/${month}/${day}`);
      console.log(response);
      if (response.status === 200) {
        setdiaPromocion(response.data)
        console.log(diaPromocion);
      }
    } catch (error) {
      console.log(error);
    }
  }
  return {
    //funciones
    buscarFechaPromocion,
    handlerOpenForm,
    handlerCloseForm,
    guardarPromocion,
    //estados
    diaPromocion,
    visibleFrom,
    errors
  }
}
