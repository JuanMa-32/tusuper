import { useReducer } from "react"
import { CarroItem } from "../reducers/CarroItem"


export const UseCarroItem = () => {
  
  const [carroItem, dispatch] = useReducer(CarroItem, [])

  const handlerAddProductCart = (product) => {
    const hasItem = carroItem.find((i) => i.product.id === product.id);
    if (hasItem) {
        dispatch({
                type: 'updateCantidadProductoCarro',
                payload: product,
            });
    } else {
        dispatch({
            type: 'addProductoCarro',
            payload: product,
        });
    }
}

const restarProducto = (product) => {
    dispatch({
            type: 'restarCantidadProductoCarro',
            payload: product,
        });
}
const restablecerCarro = () => {
    dispatch({
        type:'vaciarCarro'
    })
}

const handlerDeleteProductCart = (id) => {
    dispatch({
            type: 'deleteProductoCarro',
            payload: id,
        })
}

  
    return {
        //FUNCIONES
        restablecerCarro,
        handlerAddProductCart,
        handlerDeleteProductCart,
        restarProducto,
        //VARIABLES
        carroItem,
    }
}