import React, { useReducer, useState } from 'react'
import { cancelarVipCliente, getClientesVip, registroCliente } from '../services/ClientesService'
import Swal from 'sweetalert2'
import { ClienteReducer } from './../reducers/ClienteReducer';

export const UseCliente = () => {
    useReducer(ClienteReducer,[])
    const [clientesVip, dispatch] = useReducer(ClienteReducer,[])
   
    const [errorCliente, seterrorCliente] = useState({})

    const guardarCliente = async (cliente) => {
        try {
            const response = await registroCliente(cliente);    
            Swal.fire
            ('Exito', 'Create tu cuenta con exito', 'success') 
            seterrorCliente({})
        } catch (error) {
            if (error.response && error.response.status == 400) {
                seterrorCliente(error.response.data)
            }
        }
       
       
    }

    const buscarClientesVip = async () => {
        const response = await getClientesVip();
        dispatch({
            type:'loadingCliente',
            payload:response.data
        })
    }
    
    const darDeBajaVip = async (idCliente) => {
        const response = await cancelarVipCliente(idCliente);
        dispatch({
            type:'updateCliente',
            payload:response.data
        })
        Swal.fire
        ('Exito', 'El cliente fue cancelado como VIP.', 'success') 
      
    }

    return {
        //funciones
        buscarClientesVip,
        guardarCliente,
        darDeBajaVip,
        //estados
        clientesVip,
        errorCliente
    }
}
