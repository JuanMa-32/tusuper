import { useReducer, useState } from "react"
import { ProductoReducer } from "../reducers/ProductoReducer"
import { cambiarEstadoProducto, findAll, findAllActivos, findByCategoria, findByNombre, orderByPrecioAsc, orderByPrecioDesc, saveProducto } from "../services/ProductoService"
import { useNavigate } from "react-router-dom"
import Swal from "sweetalert2"


export const UseProducto = () => {
    const [productos, dispatch] = useReducer(ProductoReducer, [])
    const [errorProducto, seterrorProducto] = useState({})
    const nav = useNavigate()

    const guardarProducto = async (producto) => {
        try {
            const response = await saveProducto(producto)
            Swal.fire
                ('Exito', 'El Producto fue creado con exito', 'success')
            seterrorProducto({})
            nav('productos/all')
        } catch (error) {
            if (error.response && error.response.status == 400) {
                seterrorProducto(error.response.data)
            }
        }
    }
    const handlerEstadoProducto = async (id) => {
        try {
            const response = await cambiarEstadoProducto(id);
            dispatch({
                type: 'updateProducto',
                payload: response.data
            })
        } catch (error) {
            console.log(error);
        }
    }

    
    const productosActivos = async () => {
        try {
            const response = await findAllActivos();
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }
    const productosAll = async () => {
        try {
            const response = await findAll();
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }
    const productosOrdenadosXPrecioDesc = async () => {
        try {
            const response = await orderByPrecioDesc();
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }
    const productosOrdenadosXPrecioAsc = async () => {
        try {
            const response = await orderByPrecioAsc();
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }
    const buscarPorCategoria = async (categoria) => {
        try {
            const response = await findByCategoria(categoria);
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }
    const buscarPorNombre = async (nombre) => {
        try {
            const response = await findByNombre(nombre);
            dispatch({
                type: 'loadingProductos',
                payload: response.data
            })

        } catch (error) {
            console.log(error);
        }
    }

    return {
        //estados
        productos,
        errorProducto,
        //funciones
        productosActivos,
        productosOrdenadosXPrecioDesc,
        buscarPorCategoria,
        productosOrdenadosXPrecioAsc,
        buscarPorNombre,
        productosAll,
        guardarProducto,
        handlerEstadoProducto
    }
}
