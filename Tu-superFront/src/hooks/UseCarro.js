import { useState } from "react";
import { carrosClient, saveCarro } from './../services/CarroService';
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

const carroInit = {
    cliente: 0,
    subTotal: 0,
    total: 0,
    tipoCarro: '',
    items: [
        {
            producto: 0,
            cantidad: 0,
            precioUnitario: 0
        }
    ]
}

export const UseCarro = () => {
    const [carro, setcarro] = useState(carroInit)

    const [carroCliente, setcarroCliente] = useState([])
    const nav = useNavigate()

    const setCarroEstado = (items, total,subtotal,descuento, cliente, tipoCarro) => {
        console.log(items);
        const itemsDto = items.map(item => ({
            producto: item.product.id,
            cantidad: item.quantity,
            precioUnitario: item.product.precio,
           
        }));
        setcarro({
            ...carro,
            cliente: cliente,
            subtotal: subtotal,
            total: total,
            descuento:descuento,
            tipoCarro: tipoCarro,
            items: itemsDto
        })
    }

    const guardarCompraConfirmada = async() => {
        try {
            const response =await saveCarro(carro);
            Swal.fire
            ('Exito', 'El Producto fue creado con exito', 'success')
            nav('/')
        } catch (error) {
            console.log(error);
        }
    }

    const getcarrosCliente = async(idCliente) => {
        const response = await carrosClient(idCliente);
        setcarroCliente(response.data);
    }

    return {
        //estados
        carro,
        carroCliente,
        //funciones
        setCarroEstado,
        guardarCompraConfirmada,
        getcarrosCliente
    }
}
