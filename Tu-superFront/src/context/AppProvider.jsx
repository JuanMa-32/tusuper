import React from 'react'
import { AppContext } from './AppContext'
import { UseProducto } from '../hooks/UseProducto'
import { UseCarroItem } from '../hooks/UseCarroItem';
import { UseCarro } from '../hooks/UseCarro';
import { UseCliente } from '../hooks/UseCliente';
import { UseDiaPromocion } from '../hooks/UseDiaPromocion';

export const AppProvider = ({ children }) => {
    const {
        //estados
        productos,
        errorProducto,
        //funciones
        productosActivos,
        productosOrdenadosXPrecioDesc,
        buscarPorCategoria,
        productosOrdenadosXPrecioAsc,
        buscarPorNombre,
        productosAll,
        guardarProducto,
        handlerEstadoProducto
    } = UseProducto();

    const {
        //FUNCIONES
        restablecerCarro,
        handlerAddProductCart,
        handlerDeleteProductCart,
        restarProducto,
        //VARIABLES
        carroItem,
    } = UseCarroItem()

    const {
        //funciones
        buscarClientesVip,
        guardarCliente,
        darDeBajaVip,
        //estados
        clientesVip,
        errorCliente
    } = UseCliente();
    const {
        //estados
        carro,
        carroCliente,
        //funciones
        setCarroEstado,
        guardarCompraConfirmada,
        getcarrosCliente
    } = UseCarro();

    const {
        //funciones
        buscarFechaPromocion,
        handlerOpenForm,
        handlerCloseForm,
        guardarPromocion,
        //estados
        diaPromocion,
        visibleFrom,
        errors
    } = UseDiaPromocion();
    return (
        <AppContext.Provider value={
            {
                //productos
                //estados
                productos,
                errorProducto,
                //funciones
                productosActivos,
                productosOrdenadosXPrecioDesc,
                buscarPorCategoria,
                productosOrdenadosXPrecioAsc,
                buscarPorNombre,
                productosAll,
                guardarProducto,
                handlerEstadoProducto,

                //carroItems
                //FUNCIONES
                restablecerCarro,
                handlerAddProductCart,
                handlerDeleteProductCart,
                restarProducto,
                //VARIABLES
                carroItem,

                //carro
                //estados
                carro,
                carroCliente,
                //funciones
                setCarroEstado,
                guardarCompraConfirmada,
                getcarrosCliente,

                //cliente
                //funciones
                buscarClientesVip,
                guardarCliente,
                darDeBajaVip,
                //estados
                clientesVip,
                errorCliente,

                //diapromocion
                //funciones
                buscarFechaPromocion,
                handlerOpenForm,
                handlerCloseForm,
                guardarPromocion,
                //estados
                diaPromocion,
                visibleFrom,
                errors
            }
        }>
            {children}
        </AppContext.Provider>
    )
}
