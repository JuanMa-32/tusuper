import { useReducer } from "react"
import { useNavigate } from "react-router-dom"
import Swal from "sweetalert2"
import { loginUser } from './../services/loginUser';
import { loginReducer } from './../reducer/loginReducer';
import { getClienteFindById } from "../../services/ClientesService";

const initialLogin = JSON.parse(sessionStorage.getItem('login')) || {
    isAuth: false,
    user: undefined
}
export const useAuth = () => {

    const [login, dispatch] = useReducer(loginReducer, initialLogin)
    const navigateLogin = useNavigate()

    const handlerLogin = async ({ email, password }) => {

        try {
            const response = await loginUser({ email, password });

            const token = response.data.token;
            const claims = JSON.parse(window.atob(token.split(".")[1]));

            dispatch({
                type: 'login',
                payload: { email: claims.sub, isAdmin: claims.isAdmin, rol: claims.rol },
            });
            if (claims.rol == 'CLIENT') {
                const client = await getClienteFindById(claims.id);
                if (client.data.vip === true && client.data.finalVip == null) {
                    console.log('cliente vip');
                    sessionStorage.setItem('isVip', 'true');
                }
            }


            sessionStorage.setItem('isAuth', 'true');
            sessionStorage.setItem('isAdmin', `${claims.isAdmin}`);
            sessionStorage.setItem('email', `${claims.sub}`);
            sessionStorage.setItem('rol', `${claims.rol}`);
            sessionStorage.setItem('id', `${claims.id}`);
            sessionStorage.setItem('token', `Bearer ${token}`);
            if (claims.isAdmin) {
                navigateLogin('/productos/all')
            } else {
                navigateLogin('/')
            }

        } catch (error) {
            if (error.response?.status == 401) {
                Swal.fire('Error Login', 'Username o password invalidos', 'error');
            } else if (error.response?.status == 403) {
                Swal.fire('Error Login', 'No tiene acceso al recurso o permisos!', 'error');
            } else {
                throw error;
            }
        }
    }


    const handlerLogout = () => {
        dispatch({
            type: 'logout',
        });
        sessionStorage.removeItem('isAuth');
        sessionStorage.removeItem('isAdmin');
        sessionStorage.removeItem('rol');
        sessionStorage.removeItem('id');
        sessionStorage.removeItem('login');
        sessionStorage.removeItem('email');
        sessionStorage.removeItem('token');
        sessionStorage.clear();
        navigateLogin('/')
    }

    return {
        login,
        handlerLogin,
        handlerLogout
    }
}