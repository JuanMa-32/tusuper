import axios from "axios";

const ClienteApi = axios.create({
    baseURL: 'http://localhost:8080/api/clientes'
})

ClienteApi.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        'Authorization': sessionStorage.getItem('token')
    }
    return config;
})

export default ClienteApi;