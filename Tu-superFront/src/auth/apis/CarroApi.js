import axios from "axios";

const CarroApi = axios.create({
    baseURL: 'http://localhost:8080/api/carro'
})

CarroApi.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        'Authorization': sessionStorage.getItem('token')
    }
    return config;
})

export default CarroApi;