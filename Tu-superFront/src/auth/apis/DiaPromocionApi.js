import axios from "axios";

const DiaPromocionApi = axios.create({
    baseURL: 'http://localhost:8080/api/dia-promocion'
})

DiaPromocionApi.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        'Authorization': sessionStorage.getItem('token')
    }
    return config;
})

export default DiaPromocionApi;