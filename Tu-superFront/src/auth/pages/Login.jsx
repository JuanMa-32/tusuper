import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import { AppContext } from './../../context/AppContext';
import { AuthContext } from './../context/AuthContext';

export const Login = () => {
    const { guardarCliente, errorCliente } = useContext(AppContext);
    const{handlerLogin} = useContext(AuthContext)
    const [showCode, setShowCode] = useState(1);

    const [cliente, setcliente] = useState({})
    const [loginForm, setloginForm] = useState({})


    const onInputChange = ({ target }) => {
        const { name, value } = target
        setloginForm({
            ...loginForm,
            [name]: value
        })
    }
    const onInputChangeRegistro = ({ target }) => {
        const { name, value } = target
        setcliente({
            ...cliente,
            [name]: value
        })

    }

    const onSubmitRegistro = (event) => {
        event.preventDefault()
        guardarCliente(cliente)
    }
    const onSubmit = (event) => {
        event.preventDefault()
        handlerLogin(loginForm)
    }
    return (

        <div className="login-page" >
            <div className="form p-5">
                <h3 style={{ textAlign: 'center' }}>Tu Super</h3>
                <div style={{ textAlign: 'center' }} className='mt-3'>
                    <button
                        style={{

                            borderBottom: showCode === 1 ? '2px solid #74C0FC' : 'none',
                        }}
                        className="btn"
                        onClick={() => setShowCode(1)}
                    >
                        Iniciar Sesión
                    </button>
                    <button
                        style={{

                            borderBottom: showCode === 2 ? '2px solid #74C0FC' : 'none',
                        }}
                        className="btn"
                        onClick={() => setShowCode(2)}
                    >
                        Registrarme
                    </button>
                </div>

                {showCode === 1 && (
                    <>
                        <form onSubmit={onSubmit}>

                            <input
                                type="email"
                                id="email"
                                name='email'
                                value={loginForm.email}
                                onChange={onInputChange}
                                className="input mt-3 "

                                placeholder='Correo electrónico'

                            />

                            <input
                                type="password"
                                id="password"
                                name='password'
                                value={loginForm.password}
                                onChange={onInputChange}
                                className="input mt-3 mb-5"

                                placeholder='Contraseña'
                            />
                            <button type="submit" className="button">
                                Iniciar sesión
                            </button>
                        </form>
                        <div className="mt-5">
                            <a href="https://www.linkedin.com/in/juan-ag%C3%BCero-fullstack/" target="_blank" rel="noopener noreferrer">

                            </a>
                            <a href="https://github.com/JuanMa-32" target="_blank" rel="noopener noreferrer" className='mx-2'>

                            </a>
                        </div>
                    </>
                )}
                {showCode === 2 && (
                    <>
                        <form onSubmit={onSubmitRegistro}>

                            <p className='text-danger'>{errorCliente.nombre}</p>
                            <input
                                type="text"
                                name="nombre"
                                value={cliente?.nombre}
                                onChange={onInputChangeRegistro}

                                className="input  mb-2"
                                placeholder="Nombre"

                            />
                            <p className='text-danger'>{errorCliente.apellido}</p>
                            <input
                                type="text"
                                name="apellido"
                                value={cliente?.apellido}
                                onChange={onInputChangeRegistro}

                                className="input  mb-2"
                                placeholder="Apellido "

                            />
                            <p className='text-danger'>{errorCliente.email}</p>
                            <input
                                type="email"
                                id="email"
                                name='email'
                                value={cliente?.email}
                                onChange={onInputChangeRegistro}
                                className="input "

                                placeholder='Correo Electrónico'
                            />
                            <p className='text-danger'>{errorCliente.password}</p>
                            <input
                                type="password"
                                id="password"
                                name='password'
                                value={cliente?.password}
                                onChange={onInputChangeRegistro}

                                className="input  mb-5"
                                placeholder='Contraseña'
                            />
                            <button type="submit" className="button">
                                Registrarme
                            </button>
                        </form>
                        <div className="mt-5">
                            <a href="https://www.linkedin.com/in/juan-ag%C3%BCero-fullstack/" target="_blank" rel="noopener noreferrer">

                            </a>
                            <a href="https://github.com/JuanMa-32" target="_blank" rel="noopener noreferrer" className='mx-2'>

                            </a>
                        </div>
                    </>
                )}
            </div>
        </div>
    )
}
